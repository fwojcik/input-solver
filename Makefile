CFLAGS=-Wall -flto -g -ggdb3 -O3 -march=native -DCACHE_PLACENUM
FILES=game.c queue.c table.c index.c forest.c io.c
all:
	$(CC) $(CFLAGS) -o verify-solve-input solve.c $(FILES) -DFULL_VERIFY
	$(CC) $(CFLAGS) -o solve-input        solve.c $(FILES)
	$(CC) $(CFLAGS) -o play-input         play.c  $(FILES) rng.c -lm
