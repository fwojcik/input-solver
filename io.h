/*
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * See the LICENSE file for more details.
 */
void write_state_table(const char * fn);
void read_state_table(const char * fn);

void ckfread(void * ptr, size_t size, size_t nmemb, FILE * stream);
void ckfwrite(const void * ptr, size_t size, size_t nmemb, FILE * stream);

/*
 * These are more-or-less arbitrary numbers to easily allow
 * identification of Input(tm) solution state files, as well as the
 * different sections within each file.
 */
#define BASE_MAGIC        0x324118f
#define FILE_MAGIC     (((BASE_MAGIC)<<4) + 1)
#define BOARDREP_MAGIC (((BASE_MAGIC)<<4) + 2)
#define TABLE_MAGIC    (((BASE_MAGIC)<<4) + 3)
#define FOREST_MAGIC   (((BASE_MAGIC)<<4) + 4)
#define INDEX_MAGIC    (((BASE_MAGIC)<<4) + 5)
