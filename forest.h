/*
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * See the LICENSE file for more details.
 */
void init_forest(unsigned enable_solving, uint32_t tree_count);
void dump_hash_stats(void);

void record_levelmax(uint32_t tree, uint64_t maxnum);

struct species_t * alloc_tree(uint32_t tree);
void finalize_tree(uint32_t tree_num, struct species_t * s);
struct species_t * get_tree(uint32_t tree_num);
unsigned valid_tree(uint32_t tree_num);

uint64_t compute_leaf(struct species_t * s, uint64_t placenum);
uint8_t get_leaf(struct species_t * s, uint64_t n);
void set_leaf(struct species_t * s, uint64_t n);
unsigned count_leaves(const struct species_t * s, uint64_t n);

void read_forest(FILE * f);
void write_forest(FILE * f);
