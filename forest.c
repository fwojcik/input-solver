/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "game.h"
#include "index.h"
#include "forest.h"
#include "io.h"

/*
 * This file implements routines for handling a collection of trees
 * for groups of board positions. These trees are designed primarily
 * to find leaf nodes corresponding to given board positions, to
 * have their leaf nodes be incrementally set (made to exist), and to
 * be able to quickly count the leaf nodes prior to a given leaf
 * node. These trees are also designed to be very memory efficient.
 */

static struct species_t ** forest;
static uint8_t * levelmaxs;
static uint16_t * tree_species;
static uint32_t speciescnt;
static uint32_t forest_size;
static uint64_t forest_memsz;

/*
 * As described in index.c, a "placenum" is a sequence of numbers
 * describing the ordinal position of each piece given the positions
 * of all prior pieces. All of the placenums for a given group of
 * board positions can then be placed into a tree structure.
 *
 * The internal nodes of this tree do not contain any data, they only
 * hold references to their child nodes. The root node points to a
 * child node for each possible ordinal position of the first
 * piece. Each of those child nodes point to child node for each
 * possible ordinal position of the second piece *given the position
 * of the first piece* (which corresponds to its parent node). And so
 * on, for all the remaining pieces.
 *
 * Then, a "group" of placenums can consist of any number of board
 * positions with a fixed number of pieces already placed. This number
 * is the "cutoff". This also implies that each tree has the same
 * number of layers, one layer per piece past the cutoff.
 *
 * Placenum orderings do have a certain density to them, which allows
 * these trees to create a dense hash mapping.
 *
 * Leaf nodes also don't contain any data, and they don't have any
 * children, so they simply either exist or not. This allows us to
 * represent them using an array of bits. By interleaving words that
 * contain cumulative counts of set bits, the number of leaf nodes
 * that exist up to a given point can be computed in O(1) time. This
 * is referred to as a "succinct" data structure.
 *
 * Since efficient leaf counting is available, it is not very harmful
 * to allow "gaps" in the bitmap, where bits exist that correspond to
 * leaf nodes which are not legal board states. This allows the actual
 * tree structure to be _implicitly_ described, which greatly speeds
 * up finding the leaf node in the bitmap that corresponds to a given
 * placenum, as no actual tree walk needs to be performed.
 *
 * This is done by recording the maximum number of child nodes at each
 * level of the tree and then just assuming that every parent node in
 * the previous level has that number of children. Thus the leaf node
 * number is computed by treating the post-cutoff digits of the
 * placenum as a variable-base number, where the base of each digit is
 * the maximum number of child nodes at that level. The levelmult[]
 * array in each tree contains successive "powers" of these variable
 * bases.
 *
 * It also turns out that there is quite a lot of redundancy in the
 * tree structures across all the different groups of placenums. This
 * means we can save even more memory by storing only one copy of each
 * tree in a hash table, since each tree is read-only after
 * creation. This "uniquified" version of a tree is referred to here
 * as a "species".
 */

struct species_t {
    uint32_t levelmult[(2 * PIECES) - CUTOFF];
    uint32_t words;
    uint32_t nodes;
    uint32_t bits[0];
};

/*
 * blocksz is the number of bits (leaf nodes) that can be stored in a
 * block (word), and superblocksz is the number of bit blocks between
 * the words that are used for cumulative bit counts. Changing
 * superblocksz should only affect performance and memory usage, not
 * correctness or results. 7 seems to be the optimal number.
 *
 * 32 bit blocks are used because cumulative bit counts will never
 * exceed 32 bits, although they will exceed 16 bits.
 */
static const uint64_t blocksz = (sizeof(((struct species_t*)NULL)->bits[0]) * 8);
static const uint64_t superblocksz = 7;
/*
 * Since this does some dynamic memory allocation, a little sanity
 * testing is done first on tree size. Trees that exceed this size
 * become fatal errors. As shipped, the largest tree generated is
 * 25715 words, so this includes some hefty safety margin.
 */
static const uint32_t MAX_TREE_WORDS = 131072;

/* ================================================== */
/*
 * tree_count is the highest _number_ of tree that index.c promises to
 * use. Tree numbers are not dense, so there may be substantially
 * fewer than that number of trees that actually exist.
 *
 * If the game is only being played, and not being solved, then all
 * the tree data already exists and it implicitly contains all the
 * data stored in levelmaxs[].
 */
void init_forest(unsigned enable_solving, uint32_t tree_count) {
    forest_size = tree_count + 1;

    uint64_t sz = sizeof(forest[0]) + sizeof(tree_species[0]);
    if (enable_solving) {
        sz += sizeof(levelmaxs[0]) * ((2 * PIECES) - CUTOFF);
    }
    sz *= forest_size;

    fprintf(infofh, "Allocating %'15ld bytes for %'15d entries for tree lookup tables...",
            sz, forest_size);
    if (enable_solving) {
        levelmaxs = calloc(forest_size, sizeof(levelmaxs[0]) * ((2 * PIECES) - CUTOFF));
        if (levelmaxs == NULL) {
            fprintf(infofh, "failed\n");
            exit(1);
        }
    }
    forest = calloc(forest_size, sizeof(forest[0]));
    if (forest == NULL) {
        fprintf(infofh, "failed\n");
        exit(1);
    }
    tree_species = calloc(forest_size, sizeof(tree_species[0]));
    if (tree_species == NULL) {
        fprintf(infofh, "failed\n");
        exit(1);
    }
    fprintf(infofh, "OK\n");
}

/* ================================================== */
/*
 * Record the maximum number of child nodes at each
 * level of the tree.
 */
void record_levelmax(uint32_t tree, uint64_t maxnum) {
    uint8_t * levelmax = &levelmaxs[tree * ((2 * PIECES) - CUTOFF)];
    for (unsigned p = CUTOFF; p < 2 * PIECES; p++) {
        if (levelmax[p - CUTOFF] < get(maxnum, p)) {
            levelmax[p - CUTOFF] = get(maxnum, p);
        }
    }
}

/* ================================================== */
struct species_t * alloc_tree(uint32_t tree) {
    /*
     * Compute how many leaf nodes this tree has by assuming that
     * every parent node has the most child nodes it can.
     */
    uint8_t * levelmax = &levelmaxs[tree * ((2 * PIECES) - CUTOFF)];
    uint32_t nodes = 0;
    for (unsigned p = CUTOFF; p < 2*PIECES; p++) {
        nodes *= levelmax[p - CUTOFF];
        nodes += levelmax[p - CUTOFF] - 1;
    }
    /*
     * The above calculates the highest node number. Since the size of
     * the range [0, n] is n + 1, we add 1 to get the node count.
     */
    nodes++;

    /*
     * Allocate a tree that contains 1 bit per child node, and
     * contains enough space for the cumulative bit counts.
     */
    uint64_t words = (nodes + blocksz - 1) / blocksz;
    words += (words + superblocksz - 1) / superblocksz;
    if (words > MAX_TREE_WORDS) {
        fprintf(stderr, "Internal error; found tree of unlikely size.\n");
        fprintf(stderr, "If this was intentional, then increase MAX_TREE_WORDS.\n");
        exit(1);
    }
    struct species_t * s;
    uint64_t sz = sizeof(*s) + sizeof(s->bits[0]) * words;
    s = malloc(sz);
    if (s == NULL) {
        fprintf(stderr, "OOM after allocating %'15ld bytes of tree species\n", forest_memsz);
        exit(1);
    }
    forest_memsz += sz;

    s->nodes = nodes;
    s->words = words;

    /*
     * Note the implicit structure of the tree by recording the
     * successive "powers" of the variable-base node numbers.
     */
    uint32_t mult = 1;
    for (unsigned p = 2*PIECES - 1; p >= CUTOFF; p--) {
        s->levelmult[p - CUTOFF] = mult;
        mult *= levelmax[p - CUTOFF];
    }

    /*
     * Initialize all leaf nodes as not existing, and initialize all
     * superblock cumulative bit counts as 0.
     */
    memset(s->bits, 0, sizeof(s->bits[0]) * words);

    return s;
}

static uint32_t lookup_species(struct species_t * s);
static uint32_t add_species(struct species_t * s);

/*
 * After all the leaf nodes are done being added/set, compute the
 * cumulative set bit counts (leaf node counts) in each
 * superblock. Then add this species of tree to the forest if it
 * doesn't already exist.
 *
 * Note that this function takes ownership of s. It either free()s it,
 * or keeps it in the hash table.
 */
void finalize_tree(uint32_t tree_num, struct species_t * s) {
    /*
     * superblocks are arranged such that the first word/block
     * contains the count of all the set bits in all bit blocks prior
     * to it, and the next superblocksz blocks are the bits for the
     * leaf nodes in this superblock.
     *
     * This "wastes" one block per tree, since the count in the first
     * superblock is always 0, but it simplifies the logic for finding
     * the block and superblock of a given leaf node number.
     */
    uint32_t count = 0;
    uint32_t i     = 1;
    uint32_t j     = 0;
    while (i < s->words) {
        count += __builtin_popcount(s->bits[i++]);
        if ((++j == superblocksz) && (i < s->words)) {
            s->bits[i++] = count;
            j = 0;
        }
    }

    uint32_t species_num = lookup_species(s);
    if (species_num == 0) {
        tree_species[tree_num] = add_species(s);
    } else {
        tree_species[tree_num] = species_num;
        uint64_t sz = sizeof(*s) + sizeof(s->bits[0]) * s->words;
        forest_memsz -= sz;
        free(s);
    }
}

/* Check if leaf node n exists in the tree */
uint8_t get_leaf(struct species_t * s, uint64_t n) {
    unsigned bit  = n % blocksz;
    uint64_t word = n / blocksz + n / (blocksz * superblocksz) + 1;
    uint64_t v    = s->bits[word];
    return (v & (1ULL << bit)) ? 1 : 0;
}

/* Mark leaf node n as existing in the tree */
void set_leaf(struct species_t * s, uint64_t n) {
    unsigned bit   = n % blocksz;
    uint64_t word  = n / blocksz + n / (blocksz * superblocksz) + 1;
    s->bits[word] |= 1ULL << bit;
}

/*
 * Return the number of leaf nodes that exist and are lower than node
 * n, by counting the number of set bits that are in the range [0, n).
 */
unsigned count_leaves(const struct species_t * s, uint64_t n) {
    unsigned bit    = n % blocksz;
    uint64_t sblock = n / (blocksz * superblocksz);
    uint64_t word   = n / blocksz + sblock + 1;
    uint64_t sbword = sblock * (superblocksz + 1);
    unsigned count  = s->bits[sbword];
    for (uint64_t i = sbword + 1; i < word; i++) {
        count += __builtin_popcount(s->bits[i]);
    }
    count += __builtin_popcount(s->bits[word] & ((1ULL << bit) - 1));
    return count;
}

/*
 * Return the leaf node number for the given placenum by interpreting
 * the post-cutoff digits as a variable-base number. This implicitly
 * walks the simplified tree structure, taking the path set by placenum.
 */
uint64_t compute_leaf(struct species_t * s, uint64_t placenum) {
    uint64_t node = 0;
    for (unsigned p = CUTOFF; p < 2*PIECES; p++) {
        node += s->levelmult[p - CUTOFF] * get(placenum, p);
    }
    if (node >= s->nodes) {
        fprintf(stderr, "Node %ld exceeds max %d at pos %s\n",
                node, s->nodes, placestr(placenum));
        exit(1);
    }
    return node;
}

/* ================================================== */
static inline uint64_t rotr(const uint64_t x, int k) {
    return (x >> k) | (x << (64 - k));
}

/*
 * This is a quick-and-dirty hash function for helping quickly detect
 * if a tree matches one already in the forest.
 */
static uint64_t hash_species(const struct species_t * s) {
    const uint64_t mult   = 0x8d1d57672c38b9adULL;
    const uint64_t offset = rotr(~mult, 5);
    uint64_t hash         = s->words;
    for (unsigned i = 0; i < s->words; i++) {
        hash = hash * mult + offset;
        hash = rotr(hash, 51);
        hash ^= s->bits[i];
    }
    hash = hash * mult + offset;
    hash = rotr(hash, 51);
    hash = hash * mult + offset;
    return hash;
}

/*
 * This is the big forest of trees (well, hash table of tree species).
 *
 * As usual, a tree is hashed to find the most likely entry in the
 * array that it might live in.  There's no point in doing any sort of
 * chaining in the hash table, so linear probing is used to find a
 * free slot in case of a collision.
 *
 * Since this means there can only be as many species as there are
 * entries in this array, the "extra" bits in each entry are used to
 * store the "extra" hash bits that weren't used to determine the
 * array index. This reduces the impact of hash collisions, since this
 * way a memcmp() is only done if the full 64-bit hash matches.
 *
 * All species numbers start at 1, so this returns 0 if the given tree
 * is not a known species.
 */
#define SPECIES_HASHBITS 18
static uint64_t species_hash[1 << SPECIES_HASHBITS];
static uint64_t probes;
static uint64_t lookups;
static uint64_t collisions;
static uint32_t lookup_species(struct species_t * s) {
    uint64_t mask = (1ULL << SPECIES_HASHBITS) - 1;
    uint64_t hash = hash_species(s);
    lookups++;
    probes++;
    while (species_hash[hash & mask] != 0) {
        if (hash == ((hash & mask) | (species_hash[hash & mask] & ~mask))) {
            uint64_t species     = species_hash[hash & mask] & mask;
            struct species_t * t = forest[species];
            if ((s->nodes != t->nodes) || (s->words != t->words) ||
                    (memcmp(s->bits, t->bits, s->words) != 0)) {
                fprintf(stderr,
                        "Found BAD match of %ld at %016lx (curhash %016lx orighash %016lx)\n",
                        species, hash & mask, hash, hash_species(s));
                exit(1);
            }
            return species;
        }
        hash++;
        probes++;
    }
    return 0;
}

static uint32_t add_species(struct species_t * s) {
    uint64_t mask = (1ULL << SPECIES_HASHBITS) - 1;

    /* _pre_ increment, so 0 can mean not found */
    if (++speciescnt > mask) {
        fprintf(stderr, "Species hash table is full. Increase SPECIES_HASHBITS.\n");
        exit(1);
    }
    if (speciescnt >= forest_size) {
        fprintf(stderr, "Internal error; forest is full\n");
        exit(1);
    }

    /*
     * This is redundant with some of lookup_species() above, but
     * there aren't many inserts and keeping them separate functions
     * makes things easier for humans to understand.
     */
    uint64_t hash = hash_species(s);
    while (species_hash[hash & mask] != 0) {
        hash++;
        collisions++;
    }

    /*
     * Store the species number in the low bits and the rest of the
     * hash value in the high bits.
     */
    species_hash[hash & mask] = (hash & ~mask) | speciescnt;
    forest[speciescnt] = s;
    return speciescnt;
}

unsigned valid_tree(uint32_t tree_num) {
    return !!tree_species[tree_num];
}

struct species_t * get_tree(uint32_t tree_num) {
    if (tree_species[tree_num] == 0) {
        fprintf(stderr, "Internal error; desired tree %d not in forest\n", tree_num);
        exit(1);
    }
    return forest[tree_species[tree_num]];
}

void dump_hash_stats(void) {
    fprintf(infofh, "Allocated  %'15ld bytes for %'15d entries for tree species...OK\n",
            forest_memsz, speciescnt);
    fprintf(infofh, "Found %d species across %ld trees\n", speciescnt, lookups);
    fprintf(infofh, "Hash table had %ld collisions and %ld extra probes\n",
            collisions, probes - lookups);
}

/* ================================================== */
struct forest_section_header {
    uint32_t magic;
    uint16_t flags;
    uint16_t cutoff;
    uint32_t speciescnt;
    uint32_t forest_size;
};

void write_forest(FILE * f) {
    struct forest_section_header hdr;
    hdr.magic       = FOREST_MAGIC;
    hdr.flags       = 0;
    hdr.cutoff      = CUTOFF;
    hdr.speciescnt  = speciescnt;
    hdr.forest_size = forest_size;

    /* Write out the file section header */
    ckfwrite(&hdr, sizeof(hdr), 1, f);

    /* Write out the tree number -> species number mapping */
    ckfwrite(tree_species, sizeof(tree_species[0]), forest_size, f);

    /* Write out the details of each species of tree */
    for (unsigned i = 1; i <= speciescnt; i++) {
        struct species_t * s = forest[i];
        ckfwrite(&s->words,    sizeof(s->words),           1, f);
        ckfwrite(&s->nodes,    sizeof(s->nodes),           1, f);
        ckfwrite(s->levelmult, sizeof(s->levelmult),       1, f);
        ckfwrite(s->bits,      sizeof(s->bits[0]),  s->words, f);
    }
}

void read_forest(FILE * f) {
    struct forest_section_header hdr;

    /* Read in the file header */
    ckfread(&hdr, sizeof(hdr), 1, f);
    if (hdr.magic != FOREST_MAGIC) {
        fprintf(stderr, "File contains invalid forest section magic (%08x != %08x)\n",
                hdr.magic, FOREST_MAGIC);
        exit(1);
    }
    if (hdr.flags != 0) {
        fprintf(stderr, "Unknown flag(s) set in forest section\n");
        exit(1);
    }
    if (hdr.cutoff != CUTOFF) {
        fprintf(stderr, "Unknown cutoff %d (!= %d)\n", hdr.cutoff, CUTOFF);
        exit(1);
    }
    if (hdr.forest_size > forest_size) {
        fprintf(stderr, "Invalid forest size found in file: %d > %d\n",
                hdr.forest_size, forest_size);
        exit(1);
    }
    if (hdr.speciescnt > hdr.forest_size) {
        fprintf(stderr, "Invalid species count found in file: %d > %d\n",
                hdr.speciescnt, hdr.forest_size);
        exit(1);
    }

    speciescnt  = hdr.speciescnt;
    forest_size = hdr.forest_size;

    /* Read in the tree number -> species number mapping */
    ckfread(tree_species, sizeof(tree_species[0]), forest_size, f);

    /* Read in the details of each species of tree */
    for (unsigned i = 1; i <= speciescnt; i++) {
        uint32_t words;
        ckfread(&words,      sizeof(words),          1, f);
        if (words > MAX_TREE_WORDS) {
            fprintf(stderr, "Invalid tree word count found in file\n");
            exit(1);
        }
        struct species_t * s;
        uint64_t sz = sizeof(*s) + (sizeof(s->bits[0]) * words);
        s = malloc(sz);
        if (s == NULL) {
            fprintf(stderr, "OOM after allocating %'15ld bytes of tree species\n", forest_memsz);
            exit(1);
        }
        forest_memsz += sz;
        s->words = words;
        ckfread(&s->nodes,    sizeof(s->nodes),        1, f);
        ckfread(s->levelmult, sizeof(s->levelmult),    1, f);
        ckfread(s->bits,      sizeof(s->bits[0]),  words, f);
        forest[i] = s;
    }

    fprintf(infofh, "Allocated  %'15ld bytes for %'15d entries for tree species...OK\n",
            forest_memsz, speciescnt);
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
