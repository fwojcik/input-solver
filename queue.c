/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>

#include "game.h"
#include "index.h"

/*
 * A simple fixed-sized circular queue, which can use RAM if it is
 * available or a mmap()ed file if not.
 *
 * This stores 64-bit numbers representing game board positions and
 * retrieves them in a FIFO ordering. If CACHE_PLACENUM is defined,
 * then this stores and retrieves an additional 64-bit number (the
 * index of that position in the state table) per queue entry, and the
 * 2 numbers are completely opaque. If CACHE_PLACENUM is not defined,
 * then when a position is retrieved from the queue it is them passed
 * to compute_posidx(). In this way, the code that gets values from
 * the queue does not need to change based on that #define, as it will
 * always get both the position and its index. CACHE_PLACENUM, then,
 * allows for a simple CPU/memory tradeoff.
 */

struct Qentry {
    uint64_t pos;
#ifdef CACHE_PLACENUM
    uint64_t num;
#endif
};

static struct Qentry * posQ;
static uint64_t posQ_head;
static uint64_t posQ_tail;
static uint64_t posQ_depth;
static uint64_t posQ_sz;
static uint64_t maxQ;

uint64_t qlen(void) {
    return posQ_depth;
}

uint64_t qsiz(void) {
    return posQ_sz;
}

uint64_t qmax(void) {
    return maxQ;
}

uint64_t hasqpos(void) {
    return (posQ_depth != 0) ? 1 : 0;
}

void enqueue_pos(uint64_t pos, uint64_t num) {
    if (posQ_depth >= posQ_sz) {
        fprintf(stderr, "Error: queue full\n");
        exit(1);
    }
    posQ[posQ_head].pos = pos;
#ifdef CACHE_PLACENUM
    posQ[posQ_head].num = num;
#endif
    posQ_head = (posQ_head + 1) % posQ_sz;
    posQ_depth++;
    if (maxQ < posQ_depth) {
        maxQ = posQ_depth;
    }
}

void dequeue_pos(uint64_t * pos, uint64_t * num) {
    *pos = posQ[posQ_tail].pos;
#ifdef CACHE_PLACENUM
    *num = posQ[posQ_tail].num;
#else
    *num = compute_posidx(posQ[posQ_tail].pos);
#endif
    posQ_tail = (posQ_tail + 1) % posQ_sz;
    posQ_depth--;
}

/*
 * The queue size is not specified directly, but by the number of
 * stack spaces on the game board. These queue sizes are not easily
 * determinable without solving the game first, but this allows a
 * fixed-size implementation, which avoids an OOM possibility in the
 * middle of a long solution run.
 *
 * If use_mmap is true, then the queue is backed by a temporary file
 * instead of memory. This temporary file is invisible and will be
 * removed by the OS when this process terminates.
 */
void init_queue(uint32_t stack_height, unsigned use_mmap) {
    switch(stack_height) {
    case 0: posQ_sz = 1 << 26; break;
    case 1: posQ_sz = 1 << 28; break;
    case 2:
    case 3: posQ_sz = 1 << 30; break;
    default:
        fprintf(infofh, "Guessing 1<<32 queue entries. Please check queue.c. Good luck!\n");
        posQ_sz = 1ULL << 32; break;
    }

    if (use_mmap) {
        fprintf(infofh, "mmap()ing  %'15ld bytes for %'15ld queue slots for positions...",
                posQ_sz * sizeof(posQ[0]), posQ_sz);
        /*
         * XXX This is not safe if multiple copies of this run
         * simultaneously, as they may race on the same file, but this
         * is easier for debugging and multiple solvers does not seem
         * like a popular use case. :)
         */
        const char * filename = ".tmp.queue.solve";
        FILE * f = fopen(filename, "w+");
        if (!f) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "Error: %s opening temporary queue file %s\n",
                    strerror(errno), filename);
            exit(1);
        }
        unlink(filename);
        ftruncate(fileno(f), posQ_sz * sizeof(posQ[0]));
        posQ = mmap(NULL, posQ_sz * sizeof(posQ[0]),
                PROT_READ | PROT_WRITE, MAP_SHARED, fileno(f), 0);
        if (posQ == MAP_FAILED) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "Error: %s mmap()ing temporary queue file %s\n",
                    strerror(errno), filename);
            exit(1);
        }
        /* The mmap() man page says this does not destroy the mapping */
        fclose(f);
        fprintf(infofh, "OK\n");
    } else {
        fprintf(infofh, "Allocating %'15ld bytes for %'15ld queue slots for positions...",
                posQ_sz * sizeof(posQ[0]), posQ_sz);
        posQ = calloc(posQ_sz, sizeof(posQ[0]));
        if (posQ == NULL) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "OOM error allocating queue\n");
            exit(1);
        }
#ifdef __linux__
        /* Ensure we can "detect" OOM early on Linux */
        memset(posQ, 0xFF, posQ_sz * sizeof(posQ[0]));
        memset(posQ, 0, posQ_sz * sizeof(posQ[0]));
#endif
        fprintf(infofh, "OK\n");
    }
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
