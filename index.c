/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "game.h"
#include "index.h"
#include "forest.h"
#include "table.h"
#include "io.h"

/*
 * This implements a sort of minimal perfect hash function for
 * Input(tm) game states (aka "board positions") to index numbers,
 * which are used to access an array of data. Although the algorithm
 * for strongly solving a game is straightforward, it requires reading
 * and writing data for each game state. By using this hash, each game
 * state can be easily linked to an index of an array.
 *
 * Like many perfect hashes, this one needs to compute a variety of
 * lookup tables which are used to compute the index. These tables are
 * constructed in a few passes. First, all possible board states are
 * passed through count(), which allows the data structure sizes to be
 * computed. Second, the same set of game states in the same order are
 * passed to mkidx(), which records various information about them.
 * Finally, an optional step is to pass those states again through
 * ckidx(), to perform some sanity checking.
 *
 * Once that is complete, the compute_posidx() function will be able
 * to map any game state to a distinct number ("index"). This mapping
 * is dense (no skipped numbers) and bijective (there is a 1:1 mapping
 * between board positions and indices).
 */
static uint64_t legalpos_count;
static uint64_t legalpos_mkidx;
static uint64_t maxsubmask;
static uint32_t placemult[CUTOFF];
static uint32_t tree_count;
static uint64_t * leaftotal;

/*
 * Each space on the game board is assigned a number (see game.h for
 * the specific mapping used). Also, the game pieces are always
 * iterated over in a fixed arbitrary ordering.
 *
 * A piecepos is simply a list of the spaces that each piece is in,
 * that is then packed into a u64. Piecepos data is straightforward to
 * use with the game solving algorithm. When trying to enumerate board
 * positions, however, they are not very useful on their own.
 *
 * Each piece has a list of legal positions for it to be in *given the
 * positions of all prior pieces* (since two pieces generally can't
 * share a space). The current position of that piece then has an
 * index into that list. When those indices are packed together in a
 * u64, it becomes a "placenum". (The lengths of those position lists
 * are also used and stored in "maxnum" values.) A piecepos can then
 * be converted into a placenum.
 *
 * To convert a placenum into an index, it is split into two pieces at
 * a somewhat arbitrary boundary, called the "cutoff". Piece places
 * before the cutoff are used to categorize the placenum into a tree,
 * and those after the cutoff are associated with a specific leaf node
 * (and its path through the tree). This is done to group board
 * positions into more manageable chunks, and to take advantage of
 * structural redundancies across these groups.
 *
 * By counting the number of lower-numbered leaf nodes in the
 * placenum's tree, and counting the number of leaf nodes in all
 * lower-numbered trees, the placenum's ordinal index can be computed.
 *
 * To understand more about trees, see forest.c. The inner workings of
 * trees are hidden from this code.
 */

/* ================================================== */
void init_lookup(unsigned enable_solving, uint32_t * found_trees) {
    /*
     * maxnums are _lengths_ of lists of legal spaces for game
     * pieces. To determine if a piece is in the last legal space it
     * can be in, what is needed is the highest index in the list,
     * which is the length minus one. maxsubmask, then, is the value
     * to subtract from a maxnum to turn it into a list of indicies.
     */
    for (unsigned p = CUTOFF; p < (2 * PIECES); p++) {
        maxsubmask <<= BITSPERPIECE;
        maxsubmask += 1;
    }

    /*
     * To compute which tree number a placenum is in, the piece places
     * before the cutoff are simply treated as digits in a base-N
     * number. placemult, then, is just a list of powers of N.
     *
     * For simplicity, the chosen N is the highest count of spaces
     * that _any_ piece can be in. This means that the
     * placenum-to-tree mapping is not necessarily dense (there can be
     * many tree numbers that end up unused).
     */
    uint32_t maxplaces = compute_maxplaces();
    tree_count = 1;
    for (unsigned p = 0; p < CUTOFF; p++) {
        placemult[CUTOFF - p - 1] = tree_count;
        tree_count *= maxplaces;
    }
    *found_trees = tree_count;

    /*
     * leaftotal[] is a cached table of "the number of board states
     * (leaf nodes) across all lower-numbered trees".
     */
    uint64_t sz = tree_count * sizeof(leaftotal[0]);
    fprintf(infofh, "Allocating %'15ld bytes for %'15d cumulative tree sizes...",
            sz, tree_count);
    leaftotal = calloc(tree_count, sizeof(leaftotal[0]));
    if (leaftotal == NULL) {
        fprintf(infofh, "failed\n");
        exit(1);
    }
    fprintf(infofh, "OK\n");
}

/* ================================================== */
/*
 * As above, to determine the tree for a placenum treat the digits
 * before the cutoff as a base-N number.
 */
static uint32_t compute_tree(uint64_t placenum) {
    uint32_t tree = 0;
    for (unsigned i = 0; i < CUTOFF; i++) {
        tree += placemult[i] * get(placenum, i);
    }
    if (tree >= tree_count) {
        fprintf(stderr, "Impossible tree %d >= %d, pos %s\n",
                tree, tree_count, placestr(placenum));
        exit(1);
    }
    return tree;
}

static uint64_t compute_placeidx(uint64_t placenum) {
    /* Find the correct tree in the forest */
    uint32_t tree = compute_tree(placenum);
    struct species_t * s = get_tree(tree);

    /* Find the correct leaf in the tree*/
    uint64_t leafidx = compute_leaf(s, placenum);

    /* Count how many leaves are in this tree before this leaf */
    uint64_t placeidx = count_leaves(s, leafidx);

    /* Add in the number of leaves in all the trees before this one */
    placeidx += leaftotal[tree] - 1; /* leaftotal[] is off-by-1*/

#ifdef FULL_VERIFY
    /* This is the 2nd hottest function, so make this check optional */
    if (placeidx >= legalpos_count) {
        fprintf(stderr,
                "Internal error; leaf node count %ld exceeds bound %ld for pos %s tree %d\n",
                placeidx, legalpos_count, placestr(placenum), tree);
        exit(1);
    }
#endif

    /* And that is the ordinal number of this board position! */
    return placeidx;
}

/* ================================================== */
/*
 * If placenum == maxnum - 1 for pieces from CUTOFF and greater, then
 * this is the last node in this tree.
 *
 * Note that the placenum of the last piece is always 0, due to an
 * optimization in mkidx().
 */
static unsigned last_node_in_tree(uint64_t placenum, uint64_t maxnum) {
    placenum >>= (CUTOFF * BITSPERPIECE);
    maxnum   >>= (CUTOFF * BITSPERPIECE);
    maxnum    -= maxsubmask;
    maxnum     = set(maxnum, (2 * PIECES) - CUTOFF - 1, 0);
    return (maxnum == placenum) ? 1 : 0;
}

/*
 * Similarly, if placenum == 0 for pieces from CUTOFF and greater,
 * then this is the first node in this tree.
 */
static unsigned first_node_in_tree(uint64_t placenum) {
    placenum >>= (CUTOFF * BITSPERPIECE);
    return (placenum == 0) ? 1 : 0;
}

/* ================================================== */
uint64_t compute_posidx(uint64_t piecepos) {
    uint64_t placenum = compute_placenum(piecepos);
    return compute_placeidx(placenum);
}

size_t get_poscnt(void) {
    return legalpos_count;
}

/* ================================================== */
/*
 * The first pass through all board positions just gathers some
 * statistics, mostly to size data structures later.
 */
void count(uint64_t placenum, uint64_t maxnum) {
    /* This is one more board position. */
    legalpos_count++;

    /*
     * Since all possible locations of the last piece are fully
     * constrained by the positions of all earlier pieces, they can
     * always be considered consecutive. This also means the index.c
     * code can easily iterate over all of them on its own, so it only
     * needs to directly process positions where the last piece is in
     * its first legal position. So if it isn't, then do nothing.
     */
    if (get(placenum, 2 * PIECES - 1) != 0) {
        return;
    }

    /*
     * The first time we encounter this tree, record the cumulative
     * sum of the number of leaf nodes (board positions) from all
     * trees prior to this one. This value is off-by-1, which lets a
     * value of 0 mean "uninitialized", which is useful for
     * determining which trees are unused.
     */
    uint32_t tree = compute_tree(placenum);
    if (leaftotal[tree] == 0) {
        leaftotal[tree] = legalpos_count;
    }

    /*
     * In the first pass through all board positions, each tree also
     * records the length of the longest list of valid positions for
     * each piece past the cutoff. For example, it might record that,
     * in this tree, the 10th piece will never have more then 7 valid
     * positions no matter how the previous pieces are positioned.
     */
    record_levelmax(tree, maxnum);
}

/*
 * The second pass through all board positions records each one as
 * existing in its corresponding tree. This code does make some
 * assumptions about the order in which board positions are supplied
 * to it.
 */
void mkidx(uint64_t placenum, uint64_t maxnum) {
    static struct species_t * s = NULL;

    if (get(placenum, 2 * PIECES - 1) != 0) {
        return;
    }

    /*
     * If this board position (placenum) is the first one we've seen
     * in a given tree, then allocate a tree to hold data about all
     * the positions in that tree.
     *
     * Since it is expected that the internal node data of many trees
     * will be identical (IOW, this tree may end up being a tree
     * "species" that already exists), a species struct is used.
     */
    if (s == NULL) {
        uint32_t tree = compute_tree(placenum);
        if (!first_node_in_tree(placenum)) {
            fprintf(stderr, "Internal error; tree %d seems to be missing some placenums\n", tree);
            fprintf(stderr, "First placenum seen was %016lx (%s)\n", placenum, placestr(placenum));
            exit(1);
        }
        s = alloc_tree(tree);
    }

    /*
     * Find the leaf node in the tree that corresponds to this board
     * position, and verify it has not already been seen. Then record
     * at that node that this position exists, and use the subsequent
     * nodes to record all other positions that differ only in the
     * location of the last piece.
     */
    uint64_t nodenum = compute_leaf(s, placenum);
    if (get_leaf(s, nodenum) != 0) {
        fprintf(stderr, "Tree %d node %ld already set at pos %s\n",
                compute_tree(placenum), nodenum, placestr(placenum));
        exit(1);
    }
    for (uint64_t i = 0; i < get(maxnum, (2 * PIECES) - 1); i++) {
        set_leaf(s, nodenum + i);
        legalpos_mkidx++;
    }

    /*
     * If all positions for this tree have now been recorded, then
     * finalize this tree and then prepare for the next board position
     * to belong to a different tree.
     */
    if (last_node_in_tree(placenum, maxnum)) {
        uint32_t tree = compute_tree(placenum);
        finalize_tree(tree, s);
        s = NULL;
    }
}

/*
 * The optional third pass through all board positions verifies that
 * every piece position is correctly converted into legal space
 * indices, and verifies some helper functions.
 */
void ckidx(uint64_t placenum, uint64_t piecepos, uint64_t occupied) {
    DEBUG(fprintf(infofh, "%s\n", placestr(placenum)));

    /*
     * Verify that count() and mkidx() both saw the same number of
     * board positions. Only do this the 1st time ckidx() is called.
     */
    if (placenum == 0) {
        if (legalpos_count != legalpos_mkidx) {
            fprintf(stderr,
                    "Error! Found %ld positions in the count pass, but %ld in the mkidx pass!\n",
                    legalpos_count, legalpos_mkidx);
            exit(1);
        }
    }

    /*
     * Checks on helper functions, either self-consistency checks or
     * by comparing the incrementally-computed value against the value
     * computed by the helper function.
     */
    uint64_t piecepos2 = mirror(mirror(piecepos));
    if (piecepos != piecepos2) {
        fprintf(stderr, "Error! Piecepos %s converted into %s via %s\n",
                posstr(piecepos), posstr(piecepos2), posstr(mirror(piecepos)));
        exit(1);
    }
    uint64_t occupied2 = get_occupied(piecepos);
    if (occupied != occupied2) {
        fprintf(stderr, "Error! Piecepos %s occupied is %0lx != %0lx\n",
                posstr(piecepos), occupied, occupied2);
        exit(1);
    }
    uint64_t placenum2 = compute_placenum(piecepos);
    if (placenum != placenum2) {
        fprintf(stderr, "Error! Piecepos %s converted into %s != %s\n",
                posstr(piecepos), placestr(placenum2), placestr(placenum));
        exit(1);
    }

    /*
     * Find this board position's slot in the state table, and count
     * how many times we see each state slot referred to (which should
     * be only once). If some slot has been used more than once, then
     * some aspect of the table construction behind the
     * compute_placeidx() perfect hash function has gone wrong.
     */
    uint64_t idx = compute_placeidx(placenum);
    uint16_t value = table_get(idx);
    if (value != 0) {
        fprintf(stderr, "Error! State word %ld was invalid value %d!\n", idx, value);
        exit(1);
    }
    table_set(idx, value + 1);
}

/* ================================================== */
/* Compute the leaf count of the largest tree */
static uint64_t max_treesz(void) {
    uint64_t maxtreesz   = 0;
    uint64_t prevbasecnt = 0;
    for (uint32_t t = 1; t < tree_count; t++) {
        if (leaftotal[t] != 0) {
            if (maxtreesz < leaftotal[t] - prevbasecnt) {
                maxtreesz = leaftotal[t] - prevbasecnt;
            }
            prevbasecnt = leaftotal[t];
        }
    }
    return maxtreesz;
}

/* Compute how many bytes are needed to store cumulative leaf counts */
static size_t treedelta_size(void) {
    return ((64 - __builtin_clzl(max_treesz())) + 7) / 8;
}

/* ================================================== */
struct index_section_header {
    uint32_t magic;
    uint16_t flags;
    uint16_t treedeltasz;
    uint64_t legalpos;
};

void write_index(FILE * f) {
    struct index_section_header hdr;
    hdr.magic       = INDEX_MAGIC;
    hdr.flags       = 0;
    hdr.treedeltasz = treedelta_size();
    hdr.legalpos    = get_poscnt();

    /* Write out the file section header */
    ckfwrite(&hdr, sizeof(hdr), 1, f);

    /* Write out the cumulative prior leaf counts of each tree */
    uint64_t treesz, prevsz;
    prevsz = leaftotal[0];
    for (unsigned i = 1; i < tree_count; i++) {
        if (leaftotal[i] != 0) {
            if (!valid_tree(i)) {
                fprintf(stderr, "Internal error; invalid tree %d has non-zero base\n", i);
                exit(1);
            }
            treesz = leaftotal[i] - prevsz;
            prevsz = leaftotal[i];
        } else {
            if (valid_tree(i)) {
                fprintf(stderr, "Internal error; valid tree %d has zero base\n", i);
                exit(1);
            }
            treesz = 0;
        }
        ckfwrite(&treesz, hdr.treedeltasz, 1, f);
    }
}

void read_index(FILE * f) {
    struct index_section_header hdr;

    /* Read in the file header */
    ckfread(&hdr, sizeof(hdr), 1, f);
    if (hdr.magic != INDEX_MAGIC) {
        fprintf(stderr, "File contains invalid index section magic (%08x != %08x)\n",
                hdr.magic, INDEX_MAGIC);
        exit(1);
    }
    if (hdr.flags != 0) {
        fprintf(stderr, "Unknown flag(s) set in table section\n");
        exit(1);
    }
    if (hdr.treedeltasz > sizeof(uint64_t)) {
        fprintf(stderr, "Invalid tree delta size (%d > %ld) in table section\n",
                hdr.treedeltasz, sizeof(uint64_t));
        exit(1);
    }

    legalpos_count = hdr.legalpos;

    /* Read in the cumulative prior leaf counts of each tree */
    uint64_t treesz = 0, nextsz = 1;
    leaftotal[0] = nextsz;
    for (unsigned i = 1; i < tree_count; i++) {
        ckfread(&treesz, hdr.treedeltasz, 1, f);
        if (treesz != 0) {
            nextsz += treesz;
            leaftotal[i] = nextsz;
        } else {
            leaftotal[i] = 0;
        }
    }
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
