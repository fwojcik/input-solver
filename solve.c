/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <locale.h>

#include "game.h"
#include "index.h"
#include "forest.h"
#include "table.h"
#include "queue.h"
#include "io.h"
#include "staterep.h"

/*
 * This file implements the calculations for an ultra-strong solution
 * of the game of Input(tm) as well as some minor variations. The
 * solution is computed using retrograde analysis. The specific
 * algorithm, and a good amount of inspiration for this project, comes
 * from:
 *
 * "Calculating Ultra-Strong and Extended Solutions for Nine Men's
 *  Morris, Morabaraba, and Lasker Morris", by
 *    G�bor E. G�vay and G�bor Danner
 *
 * This is a straightforward "keep data for all positions online"
 * implementation, and does not try to partition the state space in
 * any real way. A machine with 64GB of RAM can solve the default set
 * of rules (3 pieces per stack space in the game). To use less
 * memory, the databases can be configured to remain on disk, but this
 * is significantly (3-4x) slower.
 */

/* ================================================== */
/*
 * To do the retrograde analysis, each position is initialized to a
 * value representing WIN/LOSS/DRAW if it is a terminal game state, or
 * the number of child moves it has if it is not.
 */
static void record_terminals(uint64_t piecepos) {
    uint16_t state;
    uint64_t posidx = compute_posidx(piecepos);
    switch (game_state(piecepos)) {
    case GAME_WIN:
        /*
         * In the game of Input(tm), given that piecepos is from the
         * POV of the player about to move, won states should be
         * unreachable, since that would mean that the opponent lost
         * at the end of their turn. That would mean they either moved
         * a captured piece or had the piece they moved end up
         * captured, both of which would be illegal moves.
         *
         * Verify that logic, since the state encoding no longer
         * allows (WIN, 0), and enforces depth parity with state type.
         */
        {
            uint64_t ppos[MAX_BACKWARD_MOVES];
            uint16_t pcnt = backward_moves(piecepos, ppos);
            if (pcnt != 0) {
                fprintf(stderr,
                        "Internal error; won game states should have no backward moves\n");
                exit(1);
            }
        }
        if (0) {
            state = encode_state(WIN, 0);
            enqueue_pos(piecepos, posidx);
        }
        state = encode_state(COUNT, 0);
        break;
    case GAME_LOSS:
        state = encode_state(LOSS, 0);
        enqueue_pos(piecepos, posidx);
        break;
    case GAME_DRAW:
        state = encode_state(COUNT, 0);
        break;
    default: {
        uint16_t count = forward_moves(piecepos, NULL);
        if (count == 0) {
            fprintf(stderr, "Internal error; no possible moves for INPROGRESS game?\n");
            exit(1);
        }
        state = encode_state(COUNT, count);
        break;
      }
    }
    table_set(posidx, state);
}

/* ==== Iterating over all possible board states ==== */
/*
 * The algorithm iterates over all board states at least 3 times; once
 * to count various aspects of the state space, so data structures can
 * be allocated accurately, once to construct the minimal perfect hash
 * for game states (see index.c for more on these passes), and once to
 * initialize the values for every state (record_terminals()).
 *
 * In the event that extended error checking has been enabled (the
 * FULL_VERIFY #define), additional passes are done to do some
 * consistency checks.
 *
 * These different passes are called "phases", and the phase
 * determines which "callback" function gets invoked by the state
 * iteration code.
 */
enum iterate_phases {
    PHASE_COUNT,
    PHASE_MAKE_INDEX,
    PHASE_VERIFY_INDEX,
    PHASE_VERIFY_MOVEGEN,
    PHASE_FIND_TERMINALS,
};

static void print_progress(uint64_t placenum, uint64_t maxnum) {
#if 1
    float pct = 0.0;
    float denom = 1.0;
    for (unsigned i = 0; i < 4; i++) {
        denom *= get(maxnum, i);
        pct   += get(placenum, i) * 100.0 / denom;
    }
    printf("%6.2f%%\r", pct);
#else
    printf("%ld/%ld\t%ld/%ld\t%ld/%ld\t%ld/%ld\r",
            get(placenum, 0), get(maxnum, 0),
            get(placenum, 1), get(maxnum, 1),
            get(placenum, 2), get(maxnum, 2),
            get(placenum, 3), get(maxnum, 3));
#endif
}

/*
 * Generate every pseudo-legal board state by taking the board, adding
 * the next piece in every legal position, and enumerating over the
 * remaining pieces. Then call the phase's function on each of those
 * board states.
 *
 * The reason these states are "pseudo-legal" is that they include
 * board states that can never be reached (for example, where every
 * piece is captured).
 *
 * The code must be somewhat careful, as it is not allowed to return
 * early (to a previous level of iteration) or get to a point in the
 * tree where there are no moves for a given piece. Paths to any such
 * invalid state need to be pruned earlier, to ensure the maxnum
 * values are correct. This allows the index table to be dense and not
 * have holes in the position numbering.
 *
 * In particular, callback functions may assume that if a given maxnum
 * is N then positions 0 through N-1 are definitely legal positions
 * for that piece (again, given the positions of prior pieces).
 */
static void iterate_piecepos(uint64_t piecepos, uint64_t occupied,uint64_t placenum,
        uint64_t maxnum, uint64_t curpiece, enum iterate_phases phase) {
    const uint32_t stackspaces = (1 << (STACKMAX + 1)) - (1 << STACKMIN);
    const uint64_t boardspaces = (1 << (BOARDMAX + 1)) - (1 << BOARDMIN);

    /*
     * If we have a valid board position (no more pieces left to
     * place), then perform the phase-specific callback function on
     * this board position.
     */
    if (curpiece >= (2 * PIECES)) {
        switch (phase) {
        case PHASE_COUNT: {
#ifdef FULL_VERIFY
            unsigned errors = validate_board(piecepos);
            if (errors != 0) {
                fprintf(stderr, "Invalid board state %s %016lx: %d errors\n",
                        posstr(piecepos), piecepos, errors);
                exit(1);
            }
#endif
                                   count(placenum, maxnum);}    break;
        case PHASE_MAKE_INDEX:     mkidx(placenum, maxnum);     break;
        case PHASE_VERIFY_INDEX:   ckidx(placenum, piecepos,
                                       occupied & boardspaces); break;
        case PHASE_VERIFY_MOVEGEN: ckmovegen(piecepos);         break;
        case PHASE_FIND_TERMINALS: record_terminals(piecepos);  break;
        }

#if 0
        static uint64_t counter;
        if ((counter % 100000) == 0) {
            printf("%ld\t%s\t%s\t%s\n", counter, posstr(piecepos), placestr(placenum), placestr(maxnum));
        }
        counter++;
#endif

        return;
    }

    /*
     * Periodically print out how far along we are for the user. 4
     * pieces is a good place, since that makes our progress
     * percentage sufficiently fine-grained enough to be
     * user-friendly.
     */
    if (curpiece == 4) {
        print_progress(placenum, maxnum);
    }

    /*
     * To more easily iterate over opponent pieces in the same order
     * as player pieces, this routine uses the bits in the occupied
     * variable that denote the _player's_ stack spaces to track
     * occupancy of the _opponent's_ stack spaces. So clear these bits
     * when we start processing opponent pieces, since the opponent's
     * pieces can't be there yet.
     */
    if (curpiece == PIECES) {
        occupied &= ~stackspaces;
    }

    uint32_t legalspaces = compute_legalspaces(curpiece, occupied);
    uint32_t validplaces = __builtin_popcount(legalspaces);
    maxnum = set(maxnum, curpiece, validplaces);

    /*
     * Then iterate over placing this piece into each of those legal
     * spaces, in order of decreasing space number, each time
     * recursing into this process for the next piece.
     */
    uint8_t place = 0;
    while (legalspaces) {
        /* __builtin_clz() is essentially 1-based, not 0-based. */
        char space = 32 - __builtin_clz(legalspaces) - 1;
        if ((curpiece >= PIECES) && (space < BOARDMIN)) {
            piecepos = set(piecepos, curpiece, MIRROR(space));
        } else {
            piecepos = set(piecepos, curpiece, space);
        }
        placenum = set(placenum, curpiece, place++);
        uint64_t nowoccupied = occupied | (1 << space);
        iterate_piecepos(piecepos, nowoccupied, placenum, maxnum, curpiece + 1, phase);
        legalspaces ^= (1 << space);
    }

    if (place != validplaces) {
        fprintf(stderr, "Internal error; did not iterate over legalspaces correctly\n");
        exit(1);
    }

    if (curpiece == 0) {
        printf("100.00%%\n");
    }
}

static void iterate(enum iterate_phases phase) {
    iterate_piecepos(0, 0, 0, 0, 0, phase);
}

/* ============= Game-solving algorithm ============= */
/*
 * The actual retrograde analysis algorithm. See the above paper for
 * details. These loops are less cleanly organized than I'd like, but
 * keeping this arrangement allows easier human comparison of this
 * code to the algorithm presented in the paper, which is listed here
 * in comments.
 */
static void solve_game(void) {
    uint64_t poscnt     = 0;
    uint64_t dtwQ_start = 0;
    uint64_t dtwQ_len   = 0;
    uint16_t lastdtw    = 0;

    /* while the priority queue is not empty do */
    while (hasqpos()) {
        /* Pop a position E from the priority queue */
        uint64_t pos, idx;
        dequeue_pos(&pos, &idx);
        uint16_t state_E = table_get(idx);

        /* Let the user know how things are going */
        if (((++poscnt % 1000000) == 0) || (qlen() == 0)) {
            /*
             * Number and percentage of board states process out of
             * all possible board states. Since some states might
             * never be reached, this may terminate before 100%.
             *
             * Size and %fullness of the position queue. Can be useful
             * for tweaking the pre-allocated queue size.
             *
             * Most recently processed DTW (Distance To Win), and the
             * percentage of states with the same DTW that have been
             * processed. Since queue fullness tends to go up and down
             * as DTW changes, this can help predict memory usage.
             */
            printf("Pos %12ld (%5.1f%%), Qdepth %10ld (%5.1f%%), last DTW %3d (%5.1f%%), curpos %s\n",
                    poscnt, 100.0*poscnt/get_poscnt(),
                    qlen(), 100.0*qlen()/qsiz(),
                    lastdtw,
                    (dtwQ_len == 0) ? 100.0 :100.0*(poscnt - dtwQ_start - 1)/dtwQ_len,
                    posstr(pos));
        }

        /* for all predecessors P of E do */
        uint64_t ppos[MAX_BACKWARD_MOVES];
        uint16_t pcnt = backward_moves(pos, ppos);
        for (unsigned i = 0; i < pcnt; i++) {
            uint64_t prevpos    = ppos[i];
            uint64_t prevposidx = compute_posidx(prevpos);
            uint16_t state_P    = table_get(prevposidx);
            DEBUG(printf("\tBPos %d, %s", i, posstr(prevpos)));

            /* if R[p] is COUNT then */
            if (decode_state_type(state_P) == COUNT) {

                /* if R[e] is a win then */
                if (decode_state_type(state_E) == WIN) {

                    /* Decrement R[p].count */
                    decr_state_num(&state_P);
                    DEBUG(printf("...child is a WIN, %d children remain", decode_state_num(state_P)));

                    /* if R[p].count = 0 then */
                    if (state_P == encode_state(COUNT, 0)) {

                        /* Keep track of changes to the DTW of states being queued */
                        if (lastdtw != decode_state_num(state_E)) {
                            dtwQ_start = poscnt;
                            dtwQ_len   = qlen();
                        }

                        /* R[p] <- value(LOSS in 1 + R[e].value.DTW) */
                        lastdtw = decode_state_num(state_E);
                        state_P = encode_state(LOSS, 1 + lastdtw);
                        DEBUG(printf("...queuing and marking as %s %d",
                                        state_type_str(state_P),
                                        decode_state_num(state_P)));

                        /* Push P into the priority queue */
                        enqueue_pos(prevpos, prevposidx);

                    } /* end if */

                    /* This stores the changes made to P into R[p] */
                    table_set(prevposidx, state_P);

                } else {

                    DEBUG(printf("...child is not a WIN (%s %d)",
                                    state_type_str(state_E),
                                    decode_state_num(state_E)));

                    /* Keep track of changes to the DTW of states being queued */
                    if (lastdtw != decode_state_num(state_E)) {
                        dtwQ_start = poscnt;
                        dtwQ_len   = qlen();
                    }

                    /* R[p] <- value(win in 1 + R[e].value.DTW) */
                    lastdtw = decode_state_num(state_E);
                    state_P = encode_state(WIN, 1 + lastdtw);
                    DEBUG(printf("...queuing and marking as %s %d",
                                    state_type_str(state_P),
                                    decode_state_num(state_P)));

                    /* Push P into the priority queue */
                    enqueue_pos(prevpos, prevposidx);

                    /* This stores the changes made to P into R[p] */
                    table_set(prevposidx, state_P);

                } /* end if */

            } else {
                DEBUG(printf("...skipping, state is %s %d", state_type_str(state_P), decode_state_num(state_P)));
            } /* end if */

            DEBUG(printf("\n"));

        } /* end for */

    } /* end while */

    printf("\n");
}

/* ========= State storage test subroutines ========== */
/*
 * This is the highest number that we promise will ever be passed to
 * encode_state(). If the game rules change, then the number might
 * need to change also.
 */
const uint64_t MAX_STATE_NUM = 184;

/*
 * In order to minimize memory wastage, state data is packed into
 * single 16-bit values via the code in staterep.h. These values are
 * further "compressed" by the state table implementation in table.c.
 *
 * These routines test the round-trip capabilities of these two
 * layers, to provide some assurance that state can be successfully
 * set and retrieved correctly.
 */
uint64_t test_state(unsigned i, enum state_type t, unsigned allow_spillover) {
    uint16_t state = encode_state(t, i);
    if (!allow_spillover && (state > 255)) {
        fprintf(stderr,
                "State encoding requires spillover table. Try again with --use-spillover.\n");
        exit(1);
    }
    if (decode_state_num(state) != i) {
        return 1;
    }
    if (decode_state_type(state) != t) {
        return 1;
    }
    if ((t == COUNT) && (i > 0)) {
        decr_state_num(&state);
        if (decode_state_num(state) != (i - 1)) {
            return 1;
        }
        if (decode_state_type(state) != t) {
            return 1;
        }
    }
    return 0;
}

void test_state_coding(unsigned allow_spillover) {
    uint64_t fails = 0;
    for (unsigned i = 0; i < MAX_STATE_NUM; i++) {
        if (i <= MAX_FORWARD_MOVES) {
            fails += test_state(i, COUNT, allow_spillover);
        }
        if ((i % 2) == 0) {
            fails += test_state(i, LOSS, allow_spillover);
        } else {
            fails += test_state(i, WIN, allow_spillover);
        }
    }

    if (fails != 0) {
        fprintf(stderr, "Verification 1 failed (%ld failures)\n", fails);
        exit(1);
    }
}

void test_state_storage(void) {
    /*
     * The strange-looking position calculations are just a sort of
     * very simple random number generator that can be replayed.
     */
    for (unsigned p = 0; p < 64; p++) {
        unsigned pos = (p * 0x91e552da7165d1b3ULL) & 63;
        table_set(pos, 55);
    }
    for (unsigned p = 0; p < 64; p++) {
        unsigned pos = (p * 0x91e552da7165d1b3ULL) & 63;
        if (table_get(pos) != 55) {
            fprintf(stderr, "Verification 2 failed (found bad value %d @ %d,%d)\n",
                    table_get(pos), p, pos);
            exit(1);
        }
    }
    /* Test spillover table */
    for (unsigned p = 0; p < 4; p++) {
        unsigned pos = (p * 0x91e552da7165d1b3ULL) & 63;
        table_set(pos, 789);
    }
    for (unsigned p = 0; p < 64; p++) {
        unsigned pos = (p * 0x91e552da7165d1b3ULL) & 63;
        uint16_t value = (p < 4) ? 789 : 55;
        if (table_get(pos) != value) {
            fprintf(stderr, "Verification 3 failed (found bad value %d != %d @ %d,%d)\n",
                    table_get(pos), value, p, pos);
            exit(1);
        }
    }
}

void verify_state_storage(unsigned use_spillover) {
    alloc_stub_tables();
    test_state_coding(use_spillover);
    test_state_storage();
    free_stub_tables();
}

/* ================================================== */
void usage(void) {
    printf("solve-input, a solver for the board game of Input(tm)\n");
    printf("\n  Usage: solve-input [options] [outputfile]\n");
    printf("    If outputfile is not specified, then data is written to statetree.dat\n");
    printf("\n  Game rules option:\n");
    printf("    --stackheight N\tAllow N pieces to simultaneously be on the stack space\n");
    printf("\n  Performance options (allowing time/memory tradeoff):\n");
    printf("    --mmap-queue\tUse an mmap()ed file to hold the position queue\n");
    printf("    --mmap-table\tUse an mmap()ed file to hold the state table\n");
    printf("\n  Advanced option:\n");
    printf("    --use-spillover\tEnable the spillover table, used for storing larger\n");
    printf("                   \tstate numbers. This should NOT be necessary or used\n");
    printf("                   \tunless you have altered the state encoding or the\n");
    printf("                   \ttable configuration.\n");
}

int main(int argc, const char * argv[]) {
    /* Unbuffer stdout always */
    setbuf(stdout, NULL);
    /* Allow printf() to provide the thousands separator */
    setlocale(LC_NUMERIC, "");
    /* Write info messages to stdout */
    init_infofile(stdout);

    /* Process command-line options */
    uint32_t stack_height  = 3;
    uint32_t mmap_queue    = 0;
    uint32_t mmap_table    = 0;
    uint32_t use_spillover = 0;
    while ((argc >= 2) && (argv[1][0] == '-') && (argv[1][1] == '-')) {
        if (!strcmp(&argv[1][2], "help")) {
            usage();
            exit(1);
        } else if (!strcmp(&argv[1][2], "stackheight")) {
            if (argc == 2) {
                fprintf(stderr, "Must supply game \"stack\" height.\n");
                exit(1);
            }
            stack_height = atoi(argv[2]);
            argc--; argv++;
        } else if (!strcmp(&argv[1][2], "mmap-queue")) {
            mmap_queue = 1;
        } else if (!strcmp(&argv[1][2], "mmap-table")) {
            mmap_table = 1;
        } else if (!strcmp(&argv[1][2], "use-spillover")) {
            use_spillover = 1;
        } else if (argv[1][2] != '\0') {
            fprintf(stderr, "Unknown option: %s\n", argv[1]);
            exit(1);
        }
        argc--; argv++;
    }

#ifdef FULL_VERIFY
    unsigned phasecnt = 8;
#else
    unsigned phasecnt = 6;
#endif
    unsigned phase = 0;

    printf("** Step %d/%d: Verifying state storage routines\n", ++phase, phasecnt);
    verify_state_storage(use_spillover);

    printf("** Step %d/%d: Initialization\n", ++phase, phasecnt);

    init_boardrep(stack_height);
    init_game(mmap_queue ? SOLVE_MMAP : SOLVE_MEM);

#ifdef FULL_VERIFY
    printf("** Step %d/%d: Verifying forward- and backward-move generation\n",
            ++phase, phasecnt);
    iterate(PHASE_VERIFY_MOVEGEN);
#endif

    printf("** Step %d/%d: Inventorying pseudo-legal board states\n", ++phase, phasecnt);
    iterate(PHASE_COUNT);

    printf("** Step %d/%d: Preparing board state lookup table\n", ++phase, phasecnt);
    iterate(PHASE_MAKE_INDEX);
    dump_hash_stats();
    /*
     * With the default encoding and the normal-ish game rules, all
     * states can fit into 8 bits, so use the simple table
     * implementation by specifying 0 spillover. If the user modifies
     * something
     */
    if (use_spillover) {
        alloc_table(get_poscnt(), 65536 * 8, mmap_table);
    } else {
        alloc_table(get_poscnt(), 0, mmap_table);
    }

#ifdef FULL_VERIFY
    //write_state_table("emptystate.dat");

    printf("** Step %d/%d: Verifying board state lookup table\n", ++phase, phasecnt);

    table_sanity_pre();

    iterate(PHASE_VERIFY_INDEX);

    table_sanity_post(get_poscnt());
#endif

    printf("** Step %d/%d: Recording board state successor counts\n", ++phase, phasecnt);
    iterate(PHASE_FIND_TERMINALS);

    printf("** Step %d/%d: Finding game solution\n", ++phase, phasecnt);
    solve_game();

    write_state_table(argv[1]);
    printf("Finished writing\n\n");

    printf("Max queue depth:  %ld (%5.1f%%)\n", qmax(), 100.0 * qmax() / qsiz());
    printf("Max spill length: %ld\n\n", table_spillcnt());

    /* The game starts with all pieces on HOMEROW */
    uint64_t startpos = 0;
    for (unsigned p = 0; p < PIECES; p++) {
        startpos = set(startpos, p, HOMEROW);
        startpos = set(startpos, p + PIECES, MIRROR(HOMEROW));
    }
    /*
     * Print out some stats about the initial game position, including
     * its game-theoretic value.
     */
    uint64_t startplacenum = compute_placenum(startpos);
    uint64_t startidx      = compute_posidx(startpos);
    uint16_t startstate    = table_get(startidx);
    printf("startingpos == %s piecepos == %s placenum == %ld idx\n",
            posstr(startpos), placestr(startplacenum), startidx);
    printf("state[startingpos] == %x == %s %d\n",
            startstate, state_type_str(startstate), decode_state_num(startstate));

    printf("\nCompleted solving game!\n");
    exit(0);
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
