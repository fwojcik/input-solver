/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdio.h>
#include <stdint.h>
#include <stddef.h>
#include <math.h>

/* This is as self-contained as practical. No game files are included. */
#include "rng.h"

/*
 * This RNG is largely based off of the xoshiro256** generator
 * (http://prng.di.unimi.it/xoshiro256starstar.c), with a small tweak
 * to try to avoid the "zeroland" issues documented in
 * https://www.pcg-random.org/posts/xoshiro-repeat-flaws.html.
 *
 * A simplistic method is used for turning the 64-bit seed into the
 * 256 bits of state that the RNG actually uses, as well as simulating
 * different sequences of random numbers.
 */

static uint64_t seed;
static uint64_t rngstate[4] = {0};

/* Decimal part of golden ratio, to make seed into a Weyl sequence */
static const uint64_t incr = 0x9E3779B97F4A7C15;
static void rng_init_state(void) {
    /* Some convenient random primes */
    seed += incr; rngstate[0] = seed * 0xc6d3b23c4cb58395ULL;
    seed += incr; rngstate[1] = seed * 0x8d1d57672c38b9adULL;
    seed += incr; rngstate[2] = seed * 0x91e552da7165d1b3ULL;
    seed += incr; rngstate[3] = seed * 0xa70ecd5b132a6e2bULL;
    /* Stir the state enough so dieharder passes */
    rng();
    rng();
    rng();
}

void rng_init(uint32_t seq) {
    if (seq) {
        seq   = seq << (32 - 8);
        seed ^= seq;
        seed  = (seed << 32) | seq;
    }
    rng_init_state();
}

void rng_seed(uint64_t s) {
    seed = s;
}

uint64_t rng_get_seed(void) {
    return seed;
}

void rng_next_seed(void) {
    seed++;
    rng_init_state();
}

static inline uint64_t rotl(const uint64_t x, int k) {
    return (x << k) | (x >> (64 - k));
}

uint64_t rng(void) {
    /* The original has:  = rotl(rngstate[1] * 5, 7) * 9; */
    const uint64_t result = rotl(rngstate[2] * 5, 7) * 9 + rngstate[0];

    const uint64_t t = rngstate[1] << 17;

    rngstate[2] ^= rngstate[0];
    rngstate[3] ^= rngstate[1];
    rngstate[1] ^= rngstate[2];
    rngstate[0] ^= rngstate[3];

    rngstate[2] ^= t;
    rngstate[3] = rotl(rngstate[3], 45);

    return result;
}

/*
 * Returns a random number in the [0, max) range, where max is a
 * 32-bit unsigned number.
 *
 * This could use rngdbl(), but this avoids floating point
 * math, and I just like it better.
 */
unsigned rng_range(unsigned max) {
    uint64_t r = rng() >> 32;
    return (r * max) >> 32;
}

/*
 * Returns a random double in the range (0,1), meaning both extremes
 * are _excluded_, and where each possible return value is the same
 * distance between the next-smaller and next-larger return values (if
 * any).
 *
 * This is interpolated from:
 *
 * "Conversion of High-Period Random Numbers to Floating Point", by
 *   JURGEN A DOORNIK, University of Oxford
 *   https://www.doornik.com/research/randomdouble.pdf
 *
 * The TWO_EPSILON value (2.22044604925031308085e-016 in decimal) is
 * twice the value of the smallest delta between two floating point
 * numbers in the range of [0.5, 1.0), written in "hex float"
 * notation. Think of it as "1.0f >> 52", or "1.0f * 2^-52".
 */
#define TWO_EPSILON 0x1p-52
double rngdbl(void) {
    uint64_t r = rng();      // Get a 64-bit random integer
    r >>= 12;                // Zero out the top 12, leaving 52 random bits
    return TWO_EPSILON * r + // Multiply by 2 ulps,
        TWO_EPSILON/2;       // and add 1 ulp
}

/*
 * Return a random number from the gamma distribution with the
 * specified alpha and a scale of 1.0. This is actually only an
 * approximation, taken from:
 *
 * "A Convenient Way of Generating Gamma Random Variables Using
 *    Generalized Exponential Distribution" by
 *   Debasis Kundu & Rameshwar D. Gupta
 *   http://home.iitk.ac.in/~kundu/paper120.pdf
 *
 * It performs extremely well for 0.0 < alpha < 0.9, and less well for
 * alpha values near 1.0.
 */
double rng_gamma(double alpha) {
    double d = 1.0334 - 0.0766 * exp(2.2942 * alpha);
    double a = pow(2, alpha) * pow(1 - exp(-d / 2), alpha);
    double b = alpha * pow(d, alpha - 1) * exp(-d);
    double c = a + b;

    while (1) {
        double U = rngdbl();
        double X;
        if (U < (a / c)) {
            X = -2 * log(1 - (pow(c * U, 1 / alpha) / 2));
        } else {
            X = -log(c * (1 - U) / (alpha * pow(d, alpha - 1)));
        }
        double thresh;
        if (X > d) {
            thresh = pow(d / X, 1 - alpha);
        } else {
            thresh = pow(X, alpha - 1) * exp(-X / 2) /
                (pow(2, alpha - 1) * pow(1 - exp(-X / 2), alpha - 1));
        }
        double V = rngdbl();
        if (V <= thresh) {
            return X;
        }
    }
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
