/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
/*
 * This file implements a simple coding scheme for packing (type,
 * number) tuples into a u16, and doing so in a way that is convenient
 * for common manipulations that solve.c needs as well as convenient
 * for compact memory storage via table.c.
 *
 * Given the default game rules, as well as stack height variations,
 * the scheme needs to encode:
 *
 * A) (COUNT, 0) through (COUNT, 12) (aka (COUNT, MAX_FORWARD_MOVES)),
 * B) All COUNT values in a nice way such that the value is easily
 * decrementable,
 * C) (WIN, odd values up to 183),
 * D) and (LOSS, even values up to 184)
 *
 * Since there are fewer than 255 different tuples to encode, the
 * encoding should not emit any u16s that exceed 255, in which case
 * the simpler and faster state table implementation may be used.
 *
 * If different game rules are implemented, then those bounds may no
 * longer hold, and this encoding scheme may need to change.
 *
 * The encoding chosen is:
 *   0   == COUNT, 0
 *   1   == COUNT, 1
 *   ...
 *   12  == COUNT, 12
 *   13  == LOSS, 0
 *   14  == WIN, 1
 *   15  == LOSS, 2
 *   16  == WIN, 3
 *   ...
 *   196 == WIN, 183
 *   197 == LOSS, 184
 *   ...
 *
 * This state encoding scheme is shared between solve.c and play.c,
 * but is basically opaque to the game rule and table code.
 */

enum state_type {
    COUNT = 0,
    WIN   = 1,
    LOSS  = 2,
};

static inline uint16_t encode_state(enum state_type type, uint16_t num) {
    switch(type) {
    case COUNT: {
        if (num > MAX_FORWARD_MOVES) {
            fprintf(stderr, "Invalid state: COUNT value cannot exceed MAX_FORWARD_MOVES\n");
            exit(1);
        }
        return num;
      }
    case WIN: {
        if ((num & 0x1) == 0) {
            fprintf(stderr, "Invalid state: WIN values must be odd\n");
            exit(1);
        }
        return MAX_FORWARD_MOVES + 1 + num;
    }
    case LOSS: {
        if ((num & 0x1) == 1) {
            fprintf(stderr, "Invalid state: LOSS values must be even\n");
            exit(1);
        }
        return MAX_FORWARD_MOVES + 1 + num;
    }
    }
    return MAX_FORWARD_MOVES + 1 + num; /* Should be unreachable */
}

static inline enum state_type decode_state_type(uint16_t state) {
    return (state <= MAX_FORWARD_MOVES) ? COUNT : (state & 0x1) + 1;
}

static inline uint16_t decode_state_num(uint16_t state) {
    return (state <= MAX_FORWARD_MOVES) ? state : state - (MAX_FORWARD_MOVES + 1);
}

static inline void decr_state_num(uint16_t * state) {
    if (decode_state_type(*state) != COUNT) {
        fprintf(stderr, "Internal error; cannot decrement non-COUNT states.\n");
        exit(1);
    }
    if (decode_state_num(*state) == 0) {
        fprintf(stderr, "Internal error; cannot decrement state num past 0.\n");
        exit(1);
    }
    (*state) -= 1;
}

static const char * wlstr[] = {
    [COUNT] = "drw",
    [WIN]   = "win",
    [LOSS]  = "los",
};

static inline const char * state_type_str(uint16_t state) {
    return wlstr[decode_state_type(state)];
}
