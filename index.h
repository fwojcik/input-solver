/*
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * See the LICENSE file for more details.
 */
void init_lookup(unsigned enable_solving, uint32_t * found_trees);

void count(uint64_t placenum, uint64_t maxnum);
void mkidx(uint64_t placenum, uint64_t maxnum);
void ckidx(uint64_t placenum, uint64_t piecepos, uint64_t occupied);
uint64_t compute_posidx(uint64_t piecepos);

size_t get_poscnt(void);

void read_index(FILE * f);
void write_index(FILE * f);

/*
 * The board state indexing is sort of 2-level; it requires a cutoff
 * point where pieces before the cutoff make an entry into the first
 * level, and pieces after it (combined with the entry from the first
 * level) make an entry into the second.
 *
 * The choice of CUTOFF is therefore somewhat arbitrary, as it should
 * only affect memory and performance. This choice seems to be best.
 *
 * It is a #define here because its value is shared between index.c
 * (where the first level is handled) and forest.c (where the second
 * level is handled).
 */
#define CUTOFF PIECES
