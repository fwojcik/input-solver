/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>

#include "game.h"
#include "queue.h"
#include "index.h"
#include "forest.h"
#include "io.h"

/*
 * This file implements the game rules for Input(tm).
 *
 * One important note about board state representation: pieces are not
 * tracked as (e.g.) red or blue, nor are they tracked as Player 1
 * vs. Player 2. Since the game is symmetrical, the same data can be
 * used to solve the game regardless of which color pieces are being
 * played, and regardless of first or second player.
 *
 * Therefore, pieces are always tracked as "current player's" (the
 * player about to make a move) vs. "opponent's".
 *
 * For more details on the two primary state representations (placenum
 * and piecepos), see the comments in index.c. The short version is
 * that piecepos is a raw "board location for each piece" and placenum
 * is a "_index_ of each piece's position given positions of prior
 * pieces".
 */

/*
 * These are the allowed on-board movements for the playing pieces
 * shipped with the board game. See game.h for the meanings of these
 * numbers.
 */
static char spaces[2*PIECES][PPLACES] = {
    /* Player's pieces */
    { 10, 13, 19, 18, 14 },
    { 10, 16, 20, 18, 15 },
    { 11, 13 ,17, 21, 14 },
    { 11, 16, 17, 21, 15 },
    { 12, 13, 19, 17, 18 },
    { 12, 14, 16, 20, 15 },
    /* Opponent's pieces will be computed from player's */
};
/* The number of pieces allowed to be on each player's "stack" space */
static uint64_t stacksz;

/* ================================================== */
/*
 * Given the list of player pieces, compute the moves of the
 * opponent's. Their pieces are symmetric to player's, accounting for
 * the fact that the opponent looks at a rotated version of the board.
 *
 * nextspace[] and prevspace[] allow easy finding of the next/previous
 * main board space a piece can be on given the space it is currently
 * on. Since only the current player's pieces can move, these are not
 * populated for the opponent's pieces.
 *
 * spacebitmap[] records all the main board spaces a piece can occupy.
 */
static char nextspace[PIECES][BOARDMAX - BOARDMIN + 1];
static char prevspace[PIECES][BOARDMAX - BOARDMIN + 1];
static uint32_t spacebitmap[2*PIECES];
void init_boardrep(uint32_t stack_height) {
    if (stack_height > MAXSTACKSIZE) {
        fprintf(stderr, "A bigger stack was requested than this binary can handle (%d > %d).\n",
                stack_height, MAXSTACKSIZE);
        fprintf(stderr, "Please recompile with a larger MAXSTACKSIZE.\n");
        exit(1);
    }
    stacksz = stack_height;

    for (unsigned p = 0; p < PIECES; p++) {
        for (unsigned s = 0; s < PPLACES; s++) {
            /* Compute spaces[] for opponent's pieces */
            spaces[p + PIECES][s] = MIRROR(spaces[p][s]);

            unsigned n = (s + 1) % PPLACES;
            nextspace[p][spaces[p][s] - BOARDMIN] = spaces[p][n];
            prevspace[p][spaces[p][n] - BOARDMIN] = spaces[p][s];
            spacebitmap[p       ] |= (1 << spaces[p       ][s]);
            spacebitmap[p+PIECES] |= (1 << spaces[p+PIECES][s]);
        }
    }
}

/* ================================================== */
/*
 * Stringify piece positions or place enumerations. Allows for up to
 * MAXCALLS calls to be used in the same printf() statement.
 */
#define MAXCALLS 4

#ifdef DODEBUG
static const char poschr[1 << BITSPERPIECE] = "-<123@@@@@ABCDEFfedcba@@@@@987>=";
#else
static const char poschr[1 << BITSPERPIECE] = ".0123@@@@@456789abcdef@@@@@3210.";
#endif

const char * posstr(uint64_t piecepos) {
    static unsigned idx;
    static char buf[MAXCALLS][2*PIECES + 2];
    char * ret = &buf[idx][0];
    for (unsigned p = 0; p < PIECES; p++) {
        ret[p]              = poschr[get(piecepos, p)];
        ret[p + PIECES + 1] = poschr[get(piecepos, p + PIECES)];
    }
    ret[PIECES]         = '|';
    ret[2 * PIECES + 1] = '\0';
    idx = (idx + 1) % MAXCALLS;
    return ret;
}

static const char placechr[1 << BITSPERPIECE] = "0123456789ABCDEFG@@@@@@@@@@@@@@@";
const char * placestr(uint64_t placenum) {
    static unsigned idx;
    static char buf[MAXCALLS][2*PIECES + 2];
    char * ret = &buf[idx][0];
    for (unsigned p = 0; p < PIECES; p++) {
        ret[p]              = placechr[get(placenum, p)];
        ret[p + PIECES + 1] = placechr[get(placenum, p + PIECES)];
    }
    ret[PIECES]         = '|';
    ret[2 * PIECES + 1] = '\0';
    idx = (idx + 1) % MAXCALLS;
    return ret;
}

/* ================================================== */
/*
 * Count the number of legal spaces that are _higher_ than the
 * space this piece is in.
 */
static uint32_t count_higher(uint32_t spaces, uint32_t loc) {
    uint32_t places = __builtin_popcount(spaces >> (loc + 1));
    return places;
}

/*
 * For each piece, compute a list of spaces it could legally be in
 * given the positions of earlier pieces, and then convert its current
 * space into the index into that list.
 *
 * This routine assumes it is given a valid board; in particular that
 * the stack space has a valid configuration.
 *
 * This is the most expensive routine in the whole program, taking
 * about 43% of runtime.
 */
uint64_t compute_placenum(uint64_t piecepos) {
    const uint64_t stackspaces = (1ULL << (stacksz + STACKMIN)) - (1ULL << STACKMIN);
    uint64_t occupied = 0;
    uint64_t placenum = 0;
    DEBUG(printf("%s\n", posstr(piecepos)));
    for (unsigned p = 0; p < PIECES; p++) {
        uint32_t loc = get(piecepos, p);

        uint32_t legalspaces = compute_legalspaces(p, occupied);
        uint32_t place       = count_higher(legalspaces, loc);

        placenum = set(placenum, p, place);
        occupied |= (1ULL << loc);
    }
    /*
     * Since stack spaces, HOMEROW, and CAPTURED are always last,
     * use the lowest bits for them always, even for the
     * opponent's pieces. This also means we mark the stack as
     * empty when we start processing the opponent's pieces.
     */
    occupied &= ~stackspaces;
    for (unsigned p = PIECES; p < 2*PIECES; p++) {
        uint32_t loc = get(piecepos, p);
        if (loc > BOARDMAX) {
            loc = MIRROR(loc);
        }

        uint32_t legalspaces = compute_legalspaces(p, occupied);
        uint32_t place       = count_higher(legalspaces, loc);

        placenum = set(placenum, p, place);
        occupied |= (1ULL << loc);
    }

    return placenum;
}

/*
 * For each piece, compute a list of spaces it could legally be in
 * given the positions of earlier pieces, and then convert its current
 * space into the index into that list.
 *
 * This routine assumes it is given a valid board; in particular that
 * the stack space has a valid configuration.
 *
 * This routine is never used in solving, or in most playing. It only
 * exists for human-friendly conversion of game state representations.
 */
uint64_t compute_piecepos(uint64_t placenum) {
    const uint32_t stackspaces = (1 << (STACKMAX + 1)) - (1 << STACKMIN);
    uint64_t piecepos = 0;
    uint32_t occupied = 0;
    uint32_t legalspaces;

    for (unsigned p = 0; p < 2 * PIECES; p++) {
        uint8_t curplace = get(placenum, p);
        uint8_t place = 0;

        if (p == PIECES) {
            occupied &= ~stackspaces;
        }

        legalspaces = compute_legalspaces(p, occupied);
        if (legalspaces == 0) {
            fprintf(stderr, "No legal spaces for piece %d remain. Invalid placenum.\n", p);
            exit(1);
        }
        uint32_t validplaces = __builtin_popcount(legalspaces);
        if (curplace >= validplaces) {
            fprintf(stderr, "Piece %d only has max valid place of %d, not %d. Invalid placenum.\n",
                    p, validplaces - 1, curplace);
            exit(1);
        }

        while (legalspaces) {
            char space = 32 - __builtin_clz(legalspaces) - 1;
            legalspaces ^= (1 << space);
            if (place++ != curplace) { continue; }
            if ((p >= PIECES) && (space < BOARDMIN)) {
                piecepos = set(piecepos, p, MIRROR(space));
            } else {
                piecepos = set(piecepos, p, space);
            }
            occupied = occupied | (1 << space);
            break;
        }
    }

    return piecepos;
}

/* ================================================== */
/*
 * Return a bitmap describing the set of pieces that are still in play
 * for the current player.
 */
static uint64_t get_piecemask(uint64_t piecepos_side) {
    uint64_t piecemask = 0;
    for (unsigned p = 0; p < PIECES; p++) {
        uint64_t loc = get(piecepos_side, p);
        if (loc != CAPTURED) {
            piecemask |= (1ULL << p);
        }
    }
    return piecemask;
}

/*
 * Return a bitmap describing the complete list of spaces on the board
 * that the given bitmap of pieces could ever possibly reach.
 */
static uint64_t possible_spaces(uint64_t piece_bitmap) {
    uint64_t space_bitmap = 0;
    while (piece_bitmap != 0) {
        uint64_t idx = __builtin_ctzl(piece_bitmap);
        for (unsigned i = 0; i < PPLACES; i++) {
            space_bitmap |= 1ULL << spaces[idx][i];
        }
        piece_bitmap ^= (1ULL << idx);
    }
    return space_bitmap;
}

/*
 * Compute if a given set of player pieces has _any_ overlapping
 * spaces with a given set of opponent pieces. This is used to detect
 * states which can only ever draw (since captures are impossible).
 */
static uint8_t pieces_overlap[(1 << PIECES)][(1 << PIECES)];
static void init_overlap(void) {
    for (uint64_t pb = 0; pb < (1ULL << PIECES); pb++) {
        uint64_t ps = possible_spaces(pb);
        for (uint64_t ob = pb; ob < (1ULL << PIECES); ob++) {
            uint64_t os = possible_spaces(ob << PIECES);
            pieces_overlap[pb][ob] = (ps & os) ? 1 : 0;
            pieces_overlap[ob][pb] = (ps & os) ? 1 : 0;
            DEBUG(printf("%d %d == %016lx %016lx == %d\n",
                            pb, ob, ps, os, pieces_overlap[pb][ob]));
        }
    }
}

static uint8_t captures_possible(uint64_t piecepos) {
    const uint64_t sidewidth    = (BITSPERPIECE * PIECES);
    const uint64_t mask         = (1ULL << sidewidth) - 1;
    uint64_t player_piecemask   = get_piecemask(piecepos & mask);
    uint64_t opponent_piecemask = get_piecemask((MIRROR(piecepos) >> sidewidth) & mask);
    return pieces_overlap[player_piecemask][opponent_piecemask];
}

/* ======= Determine game state (win/loss/draw) ===== */
/*
 * This returns the game state at the *start* of the turn of the
 * player-to-move.
 */
STATIC_ASSERT(CAPTURED == 0, "game_state() assumes place 0 is CAPTURED");
static uint8_t moves_possible(uint64_t piecepos);
enum game_states game_state(uint64_t piecepos) {
    const uint64_t sidewidth = (BITSPERPIECE * PIECES);
    const uint64_t mask      = (1ULL << sidewidth) - 1;

    /* If all of the player's pieces are captured, they have lost. */
    if ((piecepos & mask) == 0) {
        DEBUG(printf("%016lx is a LOSS\n", piecepos));
        return GAME_LOSS;
    }
    /* If all of the opponent's pieces are captured, player has won. */
    if ((mirror(piecepos) & mask) == 0) {
        DEBUG(printf("%016lx is a WIN\n", piecepos));
        return GAME_WIN;
    }
    /* If no further captures are possible, the game is drawn. */
    if (!captures_possible(piecepos)) {
        DEBUG(printf("%016lx is a DRAW\n", piecepos));
        return GAME_DRAW;
    }
    /* RULES AMBIGUITY: what happens when a player has no legal moves? */
    if (!moves_possible(piecepos)) {
        /* This counts it as a loss */
        DEBUG(printf("%016lx is a LOSS\n", piecepos));
        return GAME_LOSS;
    }
    return GAME_INPROGRESS;
}

const char * game_statestr(enum game_states state) {
    switch(state) {
    case GAME_LOSS:       return "LOST";
    case GAME_WIN:        return "WON";
    case GAME_DRAW:       return "DRAWN";
    case GAME_INPROGRESS: return "unfinished";
    }
    return "UNKNOWN";
}

/* ========= Single-piece move generation =============== */
static uint32_t legalspaces[1 << MAXSTACKSIZE][2*PIECES];
/*
 * Compute the list of legal spaces for each piece, taking into
 * account the current stack configuration from the positions of
 * earlier pieces.
 *
 * It might be that, given the locations of all previous pieces, this
 * piece must be on the stack. If so, enforce that. If not, then
 * pieces can appear on any space they can normally travel to that
 * isn't already occupied, or they could be on any unoccupied space on
 * the stack, or they could be in the captured bin or homerow.
 *
 * This code tries to strike a balance between fast and
 * human-understandable/verifiable.
 */
static void init_legalspaces(void) {
    const uint32_t stackspaces = (1 << (stacksz + STACKMIN)) - (1 << STACKMIN);
    const uint32_t captured    = (1 << CAPTURED);
    const uint32_t homerow     = (1 << HOMEROW);

    for (unsigned p = 0; p < 2*PIECES; p++) {
        uint8_t pieces_yet_to_place = (p >= PIECES) ? (2*PIECES - p) : (PIECES - p);

        for (uint32_t i = 0; i < (1 << stacksz); i++) {
            uint32_t occupied          = i << STACKMIN;
            uint32_t legalstackspaces  = stackspaces & ~occupied;

            /* Check for situations where a piece _must_ be in a stack space */
            if ((occupied & stackspaces) != 0) {
                uint8_t highest_occupied_stack_space =
                    32 - __builtin_clz(occupied & stackspaces) - STACKMIN;
                uint8_t total_pieces_on_stack =
                    __builtin_popcount(occupied & stackspaces);
                uint8_t holes_in_stack =
                    highest_occupied_stack_space - total_pieces_on_stack;

                if (pieces_yet_to_place < holes_in_stack) {
                    /* Not enough pieces to fill holes in the stack space */
                    DEBUG(printf("Piece %2d occupied %02x == lstack %02x == x %d y %d == ILLEGAL\n",
                                    p, occupied, legalstackspaces, i, p));
                    continue;
                } else if (pieces_yet_to_place == holes_in_stack) {
                    /* This piece is needed to fill holes in the stack space */
                    legalspaces[i][p] =
                        legalstackspaces &
                        ((1 << (highest_occupied_stack_space + STACKMIN)) - 1);
                    DEBUG(printf("Piece %2d occupied %02x == lstack %02x == x %d y %d == L1 %08x\n",
                                    p, occupied, legalstackspaces, i, p, legalspaces[i][p]));
                    continue;
                }
            }

            /*
             * At this point, the piece is not _required_ to be on the
             * stack. There may be some legal places on the stack it
             * could be. Compute what they are, and then add in the
             * usual list of other legal positions.
             */
            while (__builtin_popcount(legalstackspaces) > pieces_yet_to_place) {
                legalstackspaces ^= 1 << (31 - __builtin_clz(legalstackspaces));
            }
            legalspaces[i][p] = spacebitmap[p] | legalstackspaces | captured | homerow;
            DEBUG(printf("Piece %2d occupied %02x == lstack %02x == x %d y %d == L2 %08x\n",
                            p, occupied, legalstackspaces, i, p, legalspaces[i][p]));
        }
    }

    /*
     * Now that the complete list of possible legal spaces exists for
     * every piece, compute the maximum number of legal spaces across
     * all the pieces.
     */
    compute_maxplaces();
}

/*
 * Compute which spaces a piece is legally allowed to be in, given a
 * bitmask representing which board spaces are already occupied by
 * other pieces.
 */
uint32_t compute_legalspaces(uint64_t piece, uint64_t occupied) {
    const uint64_t boardspaces    = (1 << (BOARDMAX + 1)) - (1 << BOARDMIN);
    const uint32_t stackspaces    = (1 << (stacksz + STACKMIN)) - (1 << STACKMIN);
    uint32_t occupiedstackspaces  = stackspaces & occupied;
    uint32_t spaces               = legalspaces[occupiedstackspaces >> STACKMIN][piece];
    spaces &= ~(occupied & boardspaces);
#ifdef FULL_VERIFY
    if (spaces == 0) {
        fprintf(stderr, "Internal error; no legal moves found\n");
        exit(1);
    }
#endif
    return spaces;
}

/* ================================================== */
/*
 * The first time this is called, it computes the maximum number of
 * legal spaces across all the pieces. It always returns that number.
 */
uint32_t compute_maxplaces(void) {
    static uint32_t maxplaces;
    if (maxplaces == 0) {
        for (unsigned p = 0; p < 2 * PIECES; p++) {
            uint32_t spaces = compute_legalspaces(p, 0);
            uint32_t count  = __builtin_popcount(spaces);
            if (maxplaces < count) {
                maxplaces = count;
            }
        }
        fprintf(infofh, "Found %d maxplaces for pieces\n", maxplaces);
    }
    return maxplaces;
}

/* ============== Move generation =================== */
/*
 * Generate the list of all possible subsequent board states given the
 * current one, write them to nextpos if it is not NULL, and return
 * their count. This cannot generate more than MAX_FORWARD_MOVES
 * positions.
 */
uint16_t forward_moves(uint64_t piecepos, uint64_t * nextpos) {
    const uint64_t sidewidth = (BITSPERPIECE * PIECES);
    const uint64_t mask      = (1ULL << sidewidth) - 1;

    uint64_t piecepos_side   = piecepos & mask;
    uint16_t moves           = 0;

    /*
     * If we're just counting moves, then see if the current player's
     * pieces remain unchanged from the previous computation. If they
     * are, then the move count must be the same, since the opponent's
     * pieces cannot make any move invalid for the current player.
     *
     * This works since the board states are iterated over in order
     * such that all opponent piece configurations are processed
     * sequentially for any current player's piece configuration.
     */
    static uint64_t prev_piecepos_side;
    static uint16_t prev_moves;
    if (!nextpos && (piecepos_side == prev_piecepos_side)) {
        return prev_moves;
    }

    uint64_t occupied_player   = get_occupied(piecepos_side);
    uint64_t occupied_opponent = get_occupied(piecepos & ~mask);
    uint64_t stack_height      = get_curstack_count(piecepos_side);
    for (unsigned p = 0; p < PIECES; p++) {
        uint64_t loc = get(piecepos_side, p);
        /* Captured pieces can't move. */
        if (loc == CAPTURED) {
            continue;
        }
        /*
         * Pieces at the end of their movelist can always move
         * back to the homerow.
         */
        if (loc == spaces[p][PPLACES - 1]) {
            if (nextpos) {
                nextpos[moves] = mirror(set(piecepos, p, HOMEROW));
            }
            moves++;
        }
        /*
         * Additionally, pieces have exactly one on-board space they
         * may move to, but they can't move there if that space is
         * occupied by the same color piece. If an opponent's piece is
         * there, it becomes captured.
         */
        char nextloc;
        if (loc >= BOARDMIN) {
            /* On the board */
            nextloc = nextspace[p][loc - BOARDMIN];
        } else if (stacksz == 0) {
            /* On the homerow with no stack */
            nextloc = spaces[p][0];
        } else if (loc >= STACKMIN) {
            /* Only the piece on top of the stack may move */
            if (loc != (STACKMIN + stack_height - 1)) {
                continue;
            }
            nextloc = spaces[p][0];
        } else if (stack_height < stacksz) {
            /* On the homerow, and room on the stack*/
            nextloc = STACKMIN + stack_height;
        } else {
            /* On the homerow, but no room on the stack */
            continue;
        }
        if ((occupied_player & (1ULL << nextloc)) != 0) {
            /* Can't move on top of our own piece */
            continue;
        }
        /* Write out the new game state, if requested */
        if (nextpos) {
            uint64_t piecepos_new = set(piecepos, p, nextloc);
            /* If we landed on an opponent's piece, mark it as CAPTURED */
            if ((occupied_opponent & (1ULL << nextloc)) != 0) {
                for (unsigned p = PIECES; p < 2 * PIECES; p++) {
                    if (get(piecepos_new, p) == nextloc) {
                        piecepos_new = set(piecepos_new, p, MIRROR(CAPTURED));
                    }
                }
            }
            nextpos[moves] = mirror(piecepos_new);
        }
        moves++;
    }

    /* "Memoize" the result, for the performance improvement above */
    prev_piecepos_side = piecepos_side;
    prev_moves         = moves;

#ifdef FULL_VERIFY
    if (moves > MAX_FORWARD_MOVES) {
        fprintf(stderr, "Internal error; too many forward moves found\n");
        exit(1);
    }
#endif

    return moves;
}

/*
 * An abbreviated version of forward_moves() that only returns 1 or 0
 * indicating if the current player can make any moves or not.
 */
static uint8_t moves_possible(uint64_t piecepos) {
    const uint64_t sidewidth = (BITSPERPIECE * PIECES);
    const uint64_t mask      = (1ULL << sidewidth) - 1;

    uint64_t piecepos_side   = piecepos & mask;
    uint64_t occupied_player = get_occupied(piecepos_side);
    uint64_t stack_height    = get_curstack_count(piecepos_side);

    for (unsigned p = 0; p < PIECES; p++) {
        uint64_t loc = get(piecepos, p);
        char nextloc;

        /* Captured pieces can't move. */
        if (loc == CAPTURED) {
            continue;
        } else if (loc == spaces[p][PPLACES - 1]) {
            /*
             * Pieces at the end of their movement can always return
             * to the HOMEROW.
             */
            return 1;
        } else if (loc >= BOARDMIN) {
            /* On the board */
            nextloc = nextspace[p][loc - BOARDMIN];
        } else if (stacksz == 0) {
            /* On the homerow with no stack */
            nextloc = spaces[p][0];
        } else if (loc >= STACKMIN) {
            /* Only the piece on top of the stack may move */
            if (loc != (STACKMIN + stack_height - 1)) {
                continue;
            }
            nextloc = spaces[p][0];
        } else if (stack_height < stacksz) {
            /* On the homerow, and room on the stack*/
            return 1;
        } else {
            /* On the homerow, but no room on the stack */
            continue;
        }
        if ((occupied_player & (1ULL << nextloc)) == 0) {
            return 1;
        }
    }
    return 0;
}

/*
 * Generate the list of all possible previous board states given the
 * current one, write them to prevpos which must not be NULL, and
 * return their count. This cannot generate more than
 * MAX_BACKWARD_MOVES positions.
 */
uint16_t backward_moves(uint64_t piecepos, uint64_t * prevpos) {
    const uint64_t sidewidth = (BITSPERPIECE * PIECES);
    const uint64_t mask      = (1ULL << sidewidth) - 1;

    /*
     * Mirror the board so the "current player" is the player who will
     * make the move to create the initially given board state.
     */
    piecepos                 = mirror(piecepos);
    uint64_t piecepos_side   = piecepos & mask;
    uint16_t moves           = 0;

    /* If the current player has no pieces, they cannot move */
    if (piecepos_side == 0) {
        return 0;
    }

    /*
     * Compute the list of the opponent's pieces that have been
     * captured, and record each space they could have been captured
     * on if a move being reversed did the capturing.
     */
    uint16_t capture_map[BOARDMAX - BOARDMIN + 1] = {0};
    for (unsigned p = PIECES; p < 2 * PIECES; p++) {
        uint64_t loc = get(piecepos, p);
        if (MIRROR(loc) == CAPTURED) {
            for (unsigned s = 0; s < PPLACES; s++) {
                capture_map[spaces[p][s] - BOARDMIN] |= 1 << p;
            }
        }
    }

    uint64_t occupied     = get_occupied(piecepos);
    uint64_t stack_height = get_curstack_count(piecepos);
    for (unsigned p = 0; p < PIECES; p++) {
        uint64_t loc = get(piecepos_side, p);
        /* Your pieces can't become captured on your turn. */
        if (loc == CAPTURED) {
            continue;
        }
        /*
         * Pieces on the first space of their movelist could have come
         * from the top of stack if there is one, or from the homerow
         * if there isn't.
         */
        if ((loc == spaces[p][0]) &&
                ((stacksz == 0) || (stack_height < stacksz)))  {
            uint64_t baserevpos;
            if (stacksz == 0) {
                baserevpos = set(piecepos, p, HOMEROW);
            } else {
                baserevpos = set(piecepos, p, STACKMIN + stack_height);
            }
            prevpos[moves++] = baserevpos;
            /*
             * Compute the list of additional move possibilities if
             * this move could have captured a piece.
             */
            uint16_t captured_pieces = capture_map[loc - BOARDMIN];
            while (captured_pieces) {
                uint16_t cp = __builtin_ctz(captured_pieces);
                prevpos[moves++] = set(baserevpos, cp, loc);
                captured_pieces ^= (1ULL << cp);
            }
        }
        /*
         * Pieces could have come from their previous space. This also
         * applies where pieces on the homerow could have come from
         * their final space.
         */
        char prevloc;
        if (loc == HOMEROW) {
            prevloc = spaces[p][PPLACES - 1];
        } else if (loc == (STACKMIN + stack_height - 1)) {
            /*
             * Pieces on top of the stack could have always come from the
             * homerow.
             */
            prevloc = HOMEROW;
        } else if (loc >= BOARDMIN) {
            prevloc = prevspace[p][loc - BOARDMIN];
        } else {
            continue;
        }
        if ((occupied & (1ULL << prevloc)) != 0) {
            /* Can't move from on top of our own piece */
            continue;
        }
        uint64_t baserevpos = set(piecepos, p, prevloc);
        prevpos[moves++] = baserevpos;
        /*
         * Compute the list of additional move possibilities if this
         * move could have captured a piece.
         */
        if (loc >= BOARDMIN) {
            uint16_t captured_pieces = capture_map[loc - BOARDMIN];
            while (captured_pieces) {
                uint16_t cp = __builtin_ctz(captured_pieces);
                prevpos[moves++] = set(baserevpos, cp, loc);
                captured_pieces ^= (1ULL << cp);
            }
        }
    }

#ifdef FULL_VERIFY
    if (moves > MAX_BACKWARD_MOVES) {
        fprintf(stderr, "Internal error; too many backward moves found (%d > %d)\n",
                moves, MAX_BACKWARD_MOVES);
        exit(1);
    }
#endif
    return moves;
}

/* ======== Check if the given board is valid ======= */
/*
 * Compute if a given stack configuration is valid or not. Stack
 * "spaces" must be filled consecutively or not at all. Note that the
 * opponent's stack spaces appear in reverse bit order.
 */
static uint8_t stackvalidp[1 << MAXSTACKSIZE];
static uint8_t stackvalido[1 << MAXSTACKSIZE];

static void init_validstacks(void) {
    uint8_t pval = 0, oval = 0;
    for (unsigned i = 0; i <= stacksz; i++) {
        stackvalidp[pval] = 1;
        stackvalido[oval] = 1;
        pval = (pval << 1) | 1;
        oval = (oval >> 1) | (1 << (STACKMAX - STACKMIN));
    }
}

/*
 * Do some checks to see if there's a problem with the supplied board
 * state. Checks made include:
 *
 * 1) Player's pieces must be on the board or in player's spaces.
 * 2) Opponent's pieces must be on the board or in their spaces.
 * 3) Each piece on the board must be in a valid spot for that piece.
 * 4) No spot on the board may be occupied by more than one piece.
 * 5) The stack configuration is valid (no gaps/missing pieces)
 */
unsigned validate_board(uint64_t piecepos) {
    uint64_t stack    = 0;
    uint64_t occupied = 0;
    unsigned errors   = 0;

    /* Check current player's pieces */
    for (unsigned p = 0; p < PIECES; p++) {
        uint64_t loc = get(piecepos, p);
        if (loc > BOARDMAX) {
            /* Can't be in spaces that belong to opponent */
            fprintf(stderr, "player piece %d loc %ld > BOARDMAX\n", p, loc);
            errors++;
        } else if (loc >= BOARDMIN) {
            /* Must be in a valid space for that piece */
            uint64_t found = 0;
            for (unsigned s = 0; s < PPLACES; s++) {
                if (spaces[p][s] == loc) {
                    found = 1;
                    break;
                }
            }
            if (!found) {
                fprintf(stderr, "player piece %d in invalid loc %ld\n", p, loc);
                errors++;
            } else if (occupied & (1ULL << loc)) {
                /* Can't occupy same space as another piece */
                fprintf(stderr, "player piece %d in occupied loc %ld\n", p, loc);
                errors++;
            } else {
                occupied |= 1ULL << loc;
            }
        } else if (loc > HOMEROW) {
            /* On the stack */
            if (stack & (1ULL << loc)) {
                /* Can't occupy same space as another piece */
                fprintf(stderr, "player piece %d in occupied stack loc %ld\n", p, loc);
                errors++;
            } else {
                stack |= 1ULL << loc;
            }
        }
    }

    /* Check opponent's pieces */
    for (unsigned p = PIECES; p < 2 * PIECES; p++) {
        uint64_t loc = get(piecepos, p);
        if (loc < BOARDMIN) {
            /* Can't be in spaces that belong to current player */
            fprintf(stderr, "opponent piece %d loc %ld < BOARDMIN\n", p, loc);
            errors++;
        } else if (loc <= BOARDMAX) {
            /* Must be in a valid space for that piece */
            uint64_t found = 0;
            for (unsigned s = 0; s < PPLACES; s++) {
                if (spaces[p][s] == loc) {
                    found = 1;
                    break;
                }
            }
            if (!found) {
                fprintf(stderr, "opponent piece %d in invalid loc %ld\n", p, loc);
                errors++;
            } else if (occupied & (1ULL << loc)) {
                /* Can't occupy same space as another piece */
                fprintf(stderr, "opponent piece %d in occupied loc %ld\n", p, loc);
                errors++;
            } else {
                occupied |= 1ULL << loc;
            }
        } else if (loc < MIRROR(HOMEROW)) {
            /* On the stack */
            if (stack & (1ULL << loc)) {
                /* Can't occupy same space as another piece */
                fprintf(stderr, "opponent piece %d in occupied stack loc %ld\n", p, loc);
                errors++;
            } else {
                stack |= 1ULL << loc;
            }
        }
    }

    /* Stack piece configurations must be valid */
    if (stacksz > 0) {
        /* Current player's stack pieces */
        if (!stackvalidp[(stack >> STACKMIN) & ((1 << MAXSTACKSIZE) - 1)]) {
            fprintf(stderr, "player invalid stack config %lx %lx\n",
                    stack, (stack >> STACKMIN) & ((1 << (STACKMAX - STACKMIN)) - 1));
            errors++;
        }
        /* Opponent's stack pieces */
        if (!stackvalido[(stack >> MIRROR(STACKMAX)) & ((1 << MAXSTACKSIZE) - 1)]) {
            fprintf(stderr, "opponent invalid stack config %lx %lx\n",
                    stack, (stack >> MIRROR(STACKMAX)) & ((1 << (STACKMAX - STACKMIN)) - 1));
            errors++;
        }
    } else {
        if (stack != 0) {
            fprintf(stderr, "Piece appears on stack in stackless game\n");
            errors++;
        }
    }

    return errors;
}

/* ========= Validate move generation =============== */
/*
 * Search for bugs in the forward and backward move generation.
 *
 * This is designed to be called for every valid board state. It
 * generates the list of backward moves from that state, runs each of
 * those moves forward, and makes sure the given state is a valid
 * resulting one. It then does the converse of that, generates forward
 * moves from the given state, runs each of them backwards, and again
 * makes sure the given state is a valid result.
 *
 * It also checks for inconsistencies between forward_moves() and
 * moves_possible().
 */
void ckmovegen(uint64_t piecepos) {
    uint64_t bbuffer[MAX_BACKWARD_MOVES];
    uint64_t fbuffer[MAX_FORWARD_MOVES];
    uint16_t fmoves, fcount, bcount, found;
    unsigned errors;

    bcount = backward_moves(piecepos, bbuffer);
    for (uint16_t i = 0; i < bcount; i++) {
        errors = validate_board(bbuffer[i]);
        if (errors != 0) {
            fprintf(stderr,
                    "Error! Backwards move %d from %s (%s) was an invalid board state (%d errors)\n",
                    i, posstr(piecepos), posstr(bbuffer[i]), errors);
            exit(1);
        }

        fcount = forward_moves(bbuffer[i], fbuffer);

        found = 0;
        for (uint16_t j = 0; j < fcount; j++) {
            if (fbuffer[j] == piecepos) {
                found = 1;
                break;
            }
        }
        if (!found) {
            fprintf(stderr,
                    "Error! Backwards move %d from %s (%s) did not find original as forward move\n",
                    i, posstr(piecepos), posstr(bbuffer[i]));
            fprintf(stderr, "Found forwards moves from %s:\n", posstr(bbuffer[i]));
            for (uint16_t j = 0; j < fcount; j++) {
                fprintf(stderr, "%s\n", posstr(fbuffer[j]));
            }
            fprintf(stderr, "Found backwards moves from %s:\n", posstr(piecepos));
            for (uint16_t j = 0; j < bcount; j++) {
                fprintf(stderr, "%s\n", posstr(bbuffer[j]));
            }
            exit(1);
        }
    }

    fmoves = moves_possible(piecepos);
    fcount = forward_moves(piecepos, fbuffer);
    if (fmoves ^ !!fcount) {
        fprintf(stderr, "Error! forward move inconsistency (%d != %d [%d]) for %s\n",
                fmoves, !!fcount, fcount, posstr(piecepos));
        exit(1);
    }

    for (uint16_t i = 0; i < fcount; i++) {
        errors = validate_board(fbuffer[i]);
        if (errors != 0) {
            fprintf(stderr, "Error! Forwards move %d from %s (%s) was an invalid board state (%d errors)\n",
                    i, posstr(piecepos), posstr(fbuffer[i]), errors);
            exit(1);
        }

        bcount = backward_moves(fbuffer[i], bbuffer);

        found  = 0;
        for (uint16_t j = 0; j < bcount; j++) {
            if (bbuffer[j] == piecepos) {
                found = 1;
                break;
            }
        }
        if (!found) {
            fprintf(stderr,
                    "Error! Forwards move %d from %s (%s) did not find original as backward move\n",
                    i, posstr(piecepos), posstr(fbuffer[i]));
            fprintf(stderr, "Found backwards moves from %s:\n", posstr(fbuffer[i]));
            for (uint16_t j = 0; j < bcount; j++) {
                fprintf(stderr, "%s\n", posstr(bbuffer[j]));
            }
            fprintf(stderr, "Found forwards moves from %s:\n", posstr(piecepos));
            for (uint16_t j = 0; j < fcount; j++) {
                fprintf(stderr, "%s\n", posstr(fbuffer[j]));
            }
            exit(1);
        }
    }
}

/* ================================================== */
/* Top-level init functions */
FILE * infofh;

void init_infofile(FILE * f) {
    infofh = f;
}

void init_game(enum game_mode mode) {
    init_overlap();
    init_legalspaces();
    init_validstacks();

    if (mode >= SOLVE_MEM) {
        init_queue(stacksz, (mode == SOLVE_MMAP) ? 1 : 0);
    }

    uint32_t tree_count;
    init_lookup((mode >= SOLVE_MEM) ? 1 : 0, &tree_count);
    init_forest((mode >= SOLVE_MEM) ? 1 : 0, tree_count);
}

/* ================================================== */
struct boardrep_section_header {
    uint32_t magic;
    uint8_t flags;
    uint8_t pieces;
    uint8_t pplaces;
    uint8_t stacksz;
};

void write_boardrep(FILE * f) {
    struct boardrep_section_header hdr;
    hdr.magic   = BOARDREP_MAGIC;
    hdr.flags   = 0;
    hdr.pieces  = PIECES;
    hdr.pplaces = PPLACES;
    hdr.stacksz = stacksz;

    /* Write out the file section header */
    ckfwrite(&hdr, sizeof(hdr), 1, f);

    /* Write out the sequence of spaces for the player's pieces */
    ckfwrite(spaces, PPLACES, PIECES, f);

    /* Pad out to 32-bit boundary */
    size_t padlen = (4 - (((PPLACES) * (PIECES)) & 3)) & 3;
    const char padzero[3] = { 0, 0, 0 };
    ckfwrite(padzero, padlen, 1, f);
}

void read_boardrep(FILE * f) {
    struct boardrep_section_header hdr;

    /* Read in the file header */
    ckfread(&hdr, sizeof(hdr), 1, f);
    if (hdr.magic != BOARDREP_MAGIC) {
        fprintf(stderr, "File contains invalid boardrep section magic (%08x != %08x)\n",
                hdr.magic, BOARDREP_MAGIC);
        exit(1);
    }
    if (hdr.flags != 0) {
        fprintf(stderr, "Unknown flag(s) set in boardrep section\n");
        exit(1);
    }
    if (hdr.pieces != PIECES) {
        fprintf(stderr, "Incompatible board in state file\n (%d PIECES != %d)",
                hdr.pieces, PIECES );
        exit(1);
    }
    if (hdr.pplaces != PPLACES) {
        fprintf(stderr, "Incompatible board in state file\n (%d PPLACES != %d)",
                hdr.pplaces, PPLACES);
        exit(1);
    }
    if (hdr.stacksz > MAXSTACKSIZE) {
        fprintf(stderr, "Incompatible stack size in state file (%d > %d)\n",
                hdr.stacksz, MAXSTACKSIZE);
        fprintf(stderr, "Please recompile with a larger MAXSTACKSIZE\n");
        exit(1);
    }

    /* Read in the sequence of spaces for the player's pieces */
    ckfread(spaces, PPLACES, PIECES, f);

    /* Read past the padding */
    size_t padlen = (4 - (((PPLACES) * (PIECES)) & 3)) & 3;
    if (fseek(f, padlen, SEEK_CUR) != 0) {
        fprintf(stderr, "Error: cannot fseek() in file: %s\n", strerror(errno));
        exit(1);
    }

    /* Cache computations from the pieces' space sequences */
    init_boardrep(hdr.stacksz);
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
