/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
/*
 * The game has 6 different pieces per player (PIECES == 6).
 *
 * Each piece can move to 5 different places on the shared playing
 * area (PPLACES == 5).
 *
 * The code needs to handle up to 3 pieces being allowed to be stacked
 * on top of each player's "stack" space (MAXSTACKSIZE == 3).
 */
#define PIECES 6
#define PPLACES 5
#define MAXSTACKSIZE 3

#if MAXSTACKSIZE > PIECES
#error Does not make sense to have more stack spaces than pieces
#endif

/* =========== Board representation ================= */

/*

  OPPONENT

  19 20 21
  16 17 18
  13 14 15
  10 11 12

   PLAYER

*/
/*
 * Board representation is packed such that:
 *
 * (msb)  OOOOOOPPPPPP  (lsb)
 *
 * O = 5 bits for opponent piece position
 * P = 5 bits for player   piece position
 *
 * Spaces on the board are enumerated:
 *
 *  0 = player's captured pieces
 *  1 = player's homerow
 *  2 = player's stack low
 *  3 = player's stack mid
 *  4 = player's stack hi
 * 10 = board lower left
 * ...
 * 15 = board 2nd row right
 * 16 = board 3rd row left
 * ...
 * 21 = board upper right
 * 27 = opponent's stack hi
 * 28 = opponent's stack mid
 * 29 = opponent's stack low
 * 30 = opponent's homerow
 * 31 = opponent's captured pieces
 *
 * Spaces 0-7 are off of the shared part of the board
 * Spaces 8-23 are on the shared part
 * Spaces 24-31 are also off of the shared part
 *
 * This means "opponent's space X" == 31 - "player's space X", and all
 * space numbers fit in 5 bits per piece, so 60 bits is enough for all
 * the pieces.
 *
 * Board representations are always for player to move, not opponent
 * to move. After a player moves, the board representation gets
 * transformed to switch roles of "player" and "opponent", so that it
 * is still "player's" turn. This is done by the mirror() function.
 * The MIRROR() macro can be used to transform a single space number.
 */
#define BITSPERPIECE 5
#define MIRROR(x) (31 - (x))

/* Define some useful names for spaces, and some derived constants */
#define CAPTURED 0
#define HOMEROW 1
#define STACKMIN 2
#define STACKMAX (STACKMIN + MAXSTACKSIZE - 1)
#define BOARDMIN 10
#define BOARDMAX 21
#define BOARDSIZE (BOARDMAX - BOARDMIN + 1)
#define MAX_FORWARD_MOVES (2 * PIECES)
#define MAX_BACKWARD_MOVES (2 * PIECES * (PPLACES + 1))

#if STACKMAX >= BOARDMIN
#error Current board representation cannot handle that size stack
#endif

static inline uint64_t set(uint64_t b, uint64_t p, char v) {
    const uint64_t offset = p * BITSPERPIECE;
    const uint64_t mask   = ((1ULL << BITSPERPIECE) - 1) << offset;
    const uint64_t shiftv = ((uint64_t)v) << offset;
    b = b ^ ((b ^ shiftv) & mask);
    return b;
}

static inline uint64_t get(uint64_t b, uint64_t p) {
    const uint64_t mask = (1ULL << BITSPERPIECE) - 1;
    uint64_t offset = p * BITSPERPIECE;
    return (b >> offset) & mask;
}

static inline uint64_t mirror(uint64_t piecepos) {
    const uint64_t width = BITSPERPIECE * PIECES;
    const uint64_t magic = (1ULL << (width * 2)) - 1;
    uint64_t swapped = magic - piecepos;
    swapped = ((swapped >> width) | (swapped << width)) & magic;
    return swapped;
}

/* These routines assume a valid board state */
/* Returns occupied spaces _only on the board_ */
static inline uint64_t get_occupied(uint64_t piecepos) {
    const uint64_t boardspaces = (1 << (BOARDMAX + 1)) - (1 << BOARDMIN);
    uint64_t occupied = 0;
    for (unsigned p = 0; p < 2 * PIECES; p++) {
        occupied |= 1 << get(piecepos, p);
    }
    return occupied & boardspaces;
}

/* Returns number of occupied stack spaces for current player */
static inline uint64_t get_curstack_count(uint64_t piecepos) {
    const uint64_t stackspaces = (1 << (STACKMAX + 1)) - (1 << STACKMIN);
    uint64_t occupied = 0;
    /*
     * "Should" be bounded by PIECES, but this gives the same result
     * (thanks to the mask) and allows the compiler to reuse the
     * intermediate result from an adjacent get_occupied() call.
     */
    for (unsigned p = 0; p < 2 * PIECES; p++) {
        occupied |= 1 << get(piecepos, p);
    }
    return __builtin_popcount(occupied & stackspaces);
}

const char * posstr(uint64_t piecepos);
const char * placestr(uint64_t placenum);
uint64_t compute_placenum(uint64_t piecepos);
uint64_t compute_piecepos(uint64_t placenum);

enum game_states {
    GAME_INPROGRESS,
    GAME_DRAW,
    GAME_WIN,
    GAME_LOSS,
};

enum game_states game_state(uint64_t piecepos);
const char * game_statestr(enum game_states state);
uint16_t forward_moves(uint64_t piecepos, uint64_t * nextpos);
uint16_t backward_moves(uint64_t piecepos, uint64_t * prevpos);
uint32_t compute_legalspaces(uint64_t piece, uint64_t occupied);
uint32_t compute_maxplaces(void);

enum game_mode {
    PLAY_ONLY,
    SOLVE_MEM,
    SOLVE_MMAP
};

void init_boardrep(uint32_t stack_height);
void init_game(enum game_mode mode);
void init_infofile(FILE * f);

unsigned validate_board(uint64_t piecepos);
void ckmovegen(uint64_t piecepos);

void write_boardrep(FILE * f);
void read_boardrep(FILE * f);

extern FILE * infofh;

#ifdef DODEBUG
#define DEBUG(...) __VA_ARGS__
#else
#define DEBUG(...)
#endif

#define STATIC_ASSERT(c, m) _Static_assert(c, m)
