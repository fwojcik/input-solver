/*
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * See the LICENSE file for more details.
 */
void init_queue(uint32_t stack_height, unsigned use_mmap);

void enqueue_pos(uint64_t pos, uint64_t num);
void dequeue_pos(uint64_t * pos, uint64_t * num);

uint64_t hasqpos(void);
uint64_t qlen(void);
uint64_t qsiz(void);
uint64_t qmax(void);
