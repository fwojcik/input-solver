/*
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * See the LICENSE file for more details.
 */
void rng_seed(uint64_t s);
void rng_init(uint32_t seq);
void rng_next_seed(void);
uint64_t rng_get_seed(void);
uint64_t rng(void);
double rngdbl(void);
unsigned rng_range(unsigned max);
double rng_gamma(double alpha);
