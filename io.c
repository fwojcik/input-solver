/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>

#include "game.h"
#include "table.h"
#include "index.h"
#include "forest.h"
#include "io.h"

/*
 * Very simple wrapper functions for writing and reading all the data
 * representing a solved Input(tm) game.
 */

/* ================================================== */
void ckfread(void * ptr, size_t size, size_t nmemb, FILE * stream) {
    size_t ret = fread(ptr, size, nmemb, stream);
    if (ret != nmemb) {
        if (feof(stream)) {
            fprintf(stderr, "Error reading file; EOF reached before expected\n");
        } else {
            fprintf(stderr, "Error reading file: %s\n", strerror(errno));
        }
        exit(1);
    }
}

void ckfwrite(const void * ptr, size_t size, size_t nmemb, FILE * stream) {
    size_t ret = fwrite(ptr, size, nmemb, stream);
    if (ret != nmemb) {
        fprintf(stderr, "Error writing file: %s\n", strerror(errno));
        exit(1);
    }
}

/* ================================================== */
/*
 * Each different subsystem is responsible for reading and writing its
 * data to a stream. This design has the advantages of maintaining
 * separation of responsibility between them, keeping the
 * serialization routines near their data structures' implementations,
 * keeping the subsystems' private data file-scoped, and ensuring
 * changes in one representation won't affect the others. The
 * disadvantages include the fact that there is no one place/file to
 * find all the details of the file format, and there are some subtle
 * dependencies/requirements which must be documented only via
 * comments. "In a fight between comments and code, code always wins."
 *
 * Each subsystem has its own section header, and each includes its
 * own flags field. These fields are currently unused. In general,
 * these fields provide 2 functions: 1) pad out the file header to a
 * 32-bit alignment for human convenience, 2) more easily allow for
 * backwards compatibility in future versions of the code. If some
 * minor variation on file format ends up being needed, this can be
 * encoded in a flags field. Older versions of the code will reject
 * those files written in the new way, but newer versions can
 * potentially handle both variations.
 */
struct file_header {
    uint32_t magic;
    uint32_t flags;
};

void write_state_table(const char * fn) {
    if (fn == NULL) {
        fn = "statetree.dat";
    }
    fprintf(infofh, "Writing state tree to %s\n", fn);

    FILE * f = fopen(fn, "w");
    if (!f) {
        fprintf(stderr, "Failed opening state tree file\n");
        exit(1);
    }

    struct file_header hdr;
    hdr.magic = FILE_MAGIC;
    hdr.flags = 0;

    /* Write out the file header */
    ckfwrite(&hdr, sizeof(hdr), 1, f);

    /* Write out the board representation data */
    /*
     * This MUST happen before the state table is written, as reading
     * it requires this data to be able to initialize the data
     * structures to hold the state table data.
     */
    write_boardrep(f);

    /* Write out the state table */
    /*
     * This SHOULD happen as early in the file as possible, as table.c
     * mmap()s from the start of the file, so any bytes before this
     * point can bloat memory.
     */
    write_table(f);

    /* Write out the tree data */
    write_forest(f);

    /* Write out the game-state-to-index mapping table */
    write_index(f);

    fclose(f);
}

void read_state_table(const char * fn) {
    if (fn == NULL) {
        fn = "statetree.dat";
    }
    fprintf(infofh, "Reading state tree\n");

    FILE * f = fopen(fn, "r");
    if (!f) {
        fprintf(stderr, "Failed opening state tree file\n");
        exit(1);
    }

    /* Read in the file header */
    struct file_header hdr;
    ckfread(&hdr, sizeof(hdr), 1, f);
    if (hdr.magic != FILE_MAGIC) {
        fprintf(stderr, "File doesn't appear to be an Input(tm) solver file\n");
        exit(1);
    }
    if (hdr.flags != 0) {
        fprintf(stderr, "Unknown flag(s) set in file\n");
        exit(1);
    }

    /* Read in the board representation data */
    read_boardrep(f);
    /*
     * Having game initialization occur as a side-effect of reading in
     * a data file is not the cleanest design. However, the previous
     * file data is needed in order to initialize the data structures
     * for holding the following file data. It would be possible to
     * have a two-part file reading API, but that seems equally
     * inelegant, and the current symmetry across reading and writing
     * APIs is appealing.
     */
    init_game(PLAY_ONLY);

    /* Read in (mmap(), really) the state table */
    read_table(f);

    /* Read in the tree data */
    read_forest(f);

    /* Read in the game-state-to-index mapping table */
    read_index(f);

    fclose(f);
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
