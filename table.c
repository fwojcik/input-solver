/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "table.h"
#include "game.h"
#include "io.h"

/*
 * This file implements a simple enhanced array. It stores values
 * either entirely in memory or mostly in a mmap()ed file if RAM is
 * unavailable. It is designed to operate best when most or all stored
 * values are of a small magnitude (<=12 bits, say, as specified by
 * BITSPERSTATE) but where a relatively small number of values of
 * larger magnitude (16 bits) are stored in a separate "spillover"
 * table. This can save significant space over just having a normal C
 * array of full-width slots for values.
 */

/*
 * Number of bits per value in the primary table. This is a somewhat
 * arbitrary choice, but it works very well for the default/primary
 * tunable values of this board game. Changing this value should only
 * affect performance and not correctness.
 */
#define BITSPERSTATE 8

/* ================================================== */
static uint8_t * simple_state_table;
static uint64_t * state_table;
static uint64_t state_sz;
static uint16_t * state_spill;
static uint64_t spill_sz;
static uint64_t spill_next;
static const uint64_t states_per_word = 8 * sizeof(state_table[0]) / BITSPERSTATE;

/* ================================================== */
/*
 * Allocate a table with enough room for state_count values, with a
 * spillover table initially sized for init_spill_count values.
 *
 * If an init_spill_count of 0 is specified, then that means that no
 * spillover table is used, and only 8 bits of storage are
 * guaranteed. Trying to store a value larger than 255 is an error in
 * that case.
 *
 * If use_mmap is true, then the primary table is backed by a
 * temporary file instead of memory. This temporary file is invisible
 * and will be removed by the OS when this process terminates.
 *
 * On Linux, each byte of memory is touched to force the OS to
 * actually try allocating it. This lessens the odds of the OOM killer
 * reaping us only _after_ spending many hours of computation.
 */
void alloc_table(size_t state_count, size_t init_spill_count, unsigned use_mmap) {
    if ((BITSPERSTATE < 2) || (BITSPERSTATE > 15)) {
        fprintf(stderr, "Invalid BITSPERSTATE value. Please reset it and recompile.\n");
        exit(1);
    }

    state_sz = sizeof(state_table[0]) * ((state_count + states_per_word - 1) / states_per_word);

    if (use_mmap) {
        fprintf(infofh, "mmap()ing  %'15ld bytes for %'15ld game position states...",
                state_sz, state_count);
        /*
         * XXX This is not safe if multiple copies of this run
         * simultaneously, as they may race on the same file, but this
         * is easier for debugging and multiple solvers does not seem
         * like a popular use case. :)
         */
        const char * filename = ".tmp.table.solve";
        FILE * f = fopen(filename, "w+");
        if (!f) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "Error: %s opening temporary state tree file %s\n",
                    strerror(errno), filename);
            exit(1);
        }
        unlink(filename);
        ftruncate(fileno(f), state_sz);
        state_table = mmap(NULL, state_sz, PROT_WRITE, MAP_SHARED, fileno(f), 0);
        if (state_table == MAP_FAILED) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "Error: %s mmap()ing temporary state tree file %s\n",
                    strerror(errno), filename);
            exit(1);
        }
        /* The mmap() man page says this does not destroy the mapping */
        fclose(f);
        fprintf(infofh, "OK\n");
    } else {
        fprintf(infofh, "Allocating %'15ld bytes for %'15ld game position states...",
                state_sz, state_count);
        state_table = malloc(state_sz);
        if (state_table == NULL) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "OOM error allocating state tree\n");
            exit(1);
        }
#ifdef __linux__
        /* Ensure we can "detect" OOM early on Linux */
        memset(state_table, 0xFF, state_sz);
        memset(state_table, 0, state_sz);
#endif
        fprintf(infofh, "OK\n");
    }

    spill_sz = init_spill_count;
    if (spill_sz == 0) {
        /* Spill size of 0 means use simple mode */
        simple_state_table = (uint8_t *)state_table;
    } else {
        if (spill_sz >= 0xFFFFFFFF) {
            fprintf(stderr, "Requested spill table size too large for current spill encoding");
            exit(1);
        }
        fprintf(infofh, "Allocating %'15ld bytes for %'15ld extra-large states...",
                sizeof(state_spill[0]) * spill_sz, spill_sz);
        state_spill = calloc(spill_sz, sizeof(state_spill[0]));
        if (state_spill == NULL) {
            fprintf(infofh, "failed\n");
            fprintf(stderr, "OOM error allocating state tree spillover\n");
            exit(1);
        }
#ifdef __linux__
        /* Ensure we can "detect" OOM early on Linux */
        memset(state_spill, 0xFF, sizeof(state_spill[0]) * spill_sz);
        memset(state_spill, 0, sizeof(state_spill[0]) * spill_sz);
#endif
        fprintf(infofh, "OK\n");
    }
}

/*
 * This allocates a small set of tables in order to do some
 * self-tests.
 */
void alloc_stub_tables(void) {
    state_sz = 65536;
    state_table = calloc(state_sz, sizeof(state_table[0]));
    if (state_table == NULL) {
        fprintf(stderr, "state table alloc failed\n");
        exit(1);
    }
    spill_sz = 65536;
    state_spill = calloc(spill_sz, sizeof(state_spill[0]));
    if (state_spill == NULL) {
        fprintf(stderr, "spill table alloc failed\n");
        exit(1);
    }
}

void free_stub_tables(void) {
    free(state_spill);
    free(state_table);
    state_spill = NULL;
    state_table = NULL;
    state_sz    = 0;
    spill_sz    = 0;
    spill_next  = 0;
}

/* ================================================== */
/*
 * The primary table stores multiple values per u64. When _any_ of
 * those values exceeds (roughly) what can be stored in BITSPERSTATE
 * then _every_ value in that u64 gets migrated to consecutive slots
 * in the spillover table. The original u64 is used to store the first
 * slot index in the spillover table, as well as a magic bit pattern
 * (number) so that it can be recognized as a spillover index.
 *
 * If BITSPERSTATE is not 8, then the top bit of every u64 in the
 * primary table will be unused (and therefore unset) by any real
 * value being stored there, so any SPILL_MAGIC with the high bit set
 * would be OK to use.
 *
 * If BITSPERSTATE is 8, then it is important to pick a SPILL_MAGIC
 * that cannot occur naturally. This implies the full range of
 * BITSPERSTATE values shouldn't be stored directly. The all-1s value
 * could be used for the magic number, but the value below gives more
 * confidence against matches due to bugs, which could lead to
 * silently incorrect behavior/results. This does mean that any value
 * over 0xFD should be stored in the spillover table.
 */
static const uint64_t SPILL_MAGIC = 0xFEFEFEFE;
static const uint16_t PRIMARY_MAXVAL =
    (BITSPERSTATE == 8) ? 0xFD : ((1 << BITSPERSTATE) - 1);

static uint64_t make_spillover(uint64_t index) {
    return index | (SPILL_MAGIC << 32);
}

static unsigned is_spillover(uint64_t word) {
    return ((word >> 32) == SPILL_MAGIC) ? 1 : 0;
}

static unsigned get_spill_idx(uint64_t word) {
    return word & 0xFFFFFFFF;
}

uint64_t table_spillcnt(void) {
    return spill_next;
}

uint16_t table_get(uint64_t slotnum) {
    if (simple_state_table) {
        return simple_state_table[slotnum];
    }

    uint64_t word   = slotnum / states_per_word;
    uint64_t offset = slotnum % states_per_word;
    uint64_t statew = state_table[word];
    if (is_spillover(statew)) {
        return state_spill[get_spill_idx(statew) + offset];
    }
    uint64_t mask   = (1ULL << BITSPERSTATE) - 1;
    uint64_t value  = statew >> (offset * BITSPERSTATE);
    return value & mask;
}

void table_set(uint64_t slotnum, uint16_t value) {
    if (simple_state_table) {
#ifdef FULL_VERIFY
        if (value > 255) {
            fprintf(stderr,
                    "Internal error; large values require a spillover table\n");
            exit(1);
        }
#endif
        simple_state_table[slotnum] = value;
        return;
    }

    uint64_t word   = slotnum / states_per_word;
    uint64_t offset = slotnum % states_per_word;
    uint64_t statew = state_table[word];

    /*
     * If the primary slot is already spilled over, then just use the
     * spillover table.
     */
    if (is_spillover(statew)) {
        state_spill[get_spill_idx(statew) + offset] = value;
        return;
    }

    /*
     * If the given value can fit in its spot in the normal state
     * table, then just put it there.
     */
    if (value <= PRIMARY_MAXVAL) {
        uint64_t mask = (1ULL << BITSPERSTATE) - 1;
        uint64_t v    = (uint64_t)value;
        v    <<= offset * BITSPERSTATE;
        mask <<= offset * BITSPERSTATE;
        state_table[word] = (statew & ~mask) | (v & mask);
        return;
    }

    /*
     * If it can't fit, migrate this word to the spillover table,
     * after making sure there's room.
     */
    if ((spill_next + states_per_word) > spill_sz) {
        /*
         * This realloc() pattern is safe only because OOM is
         * completely fatal in this case.
         */
        uint64_t increase = (spill_sz / states_per_word) * 35/100 * states_per_word;
        fprintf(infofh, "Allocating %'15ld bytes for %'15ld additional extra-large states...",
                sizeof(state_spill[0]) * increase, increase);
        state_spill = realloc(state_spill,
                (spill_sz + increase) * sizeof(state_spill[0]));
        if (state_spill == NULL) {
            fprintf(infofh, "failed!\n");
            fprintf(stderr, "OOM error reallocating state tree spillover\n");
            exit(1);
        }
#ifdef __linux__
        /* Ensure we can "detect" OOM early on Linux */
        memset(&state_spill[spill_sz], 0xFF, increase * sizeof(state_spill[0]));
        memset(&state_spill[spill_sz], 0, increase * sizeof(state_spill[0]));
#endif
        fprintf(infofh, "OK\n");
        spill_sz += increase;
    }

    /*
     * Move every value in that state word over to the spillover
     * table. Then use that state word to store the first entry in the
     * spill table that was just used.
     */
    uint64_t mask = (1ULL << BITSPERSTATE) - 1;
    for (uint64_t off = 0; off < states_per_word; off++) {
        state_spill[spill_next + off] = statew & mask;
        statew >>= BITSPERSTATE;
    }
    /* Do this separately to keep any if() out of the for() loop above. */
    state_spill[spill_next + offset] = value;

    state_table[word] = make_spillover(spill_next);
    spill_next += states_per_word;
    return;
}

/* ================================================== */
/*
 * Some quick-and-dirty validation functions to make sure every table
 * value starts at 0, and that every value ends at 1. There is an
 * assumption there that every table index was deliberately set to
 * 1. ckidx() iterates over every position and sets the state of its
 * corresponding index to 1, so this helps provide assurance that 1)
 * each position index is unique, 2) each position index is used (the
 * mapping is dense), 3) each position is iterated over, and 4) that
 * the table set and get functionality works.
 */
void table_sanity_pre(void) {
    unsigned failed = 0;
    for (uint64_t i = 0; i < state_sz / sizeof(state_table[0]); i++) {
        if (state_table[i] != 0) {
            fprintf(stderr, "Error! Tree word %ld (%p) was invalid value %ld!\n",
                    i, &state_table[i], state_table[i]);
            failed = 1;
        }
    }
    if (failed) {
        exit(1);
    }
}

void table_sanity_post(size_t state_count) {
    unsigned failed = 0;
    for (uint64_t i = 0; i < state_count; i++) {
        if (table_get(i) != 1) {
            fprintf(stderr, "Error! State for pos %ld was invalid value %d!\n",
                    i, table_get(i));
            failed = 1;
        }
    }
    if (failed) {
        exit(1);
    }
}

/* ================================================== */
struct table_section_header {
    uint32_t magic;
    uint16_t flags;
    uint16_t bitsperstate;
    uint64_t statesz;
    uint64_t spillsz;
};

void write_table(FILE * f) {
    struct table_section_header hdr;
    hdr.magic        = TABLE_MAGIC;
    hdr.flags        = 0;
    hdr.bitsperstate = BITSPERSTATE;
    hdr.statesz      = state_sz;
    hdr.spillsz      = spill_next;

    /* Write out the file section header */
    ckfwrite(&hdr, sizeof(hdr), 1, f);

    /* Write out the state table */
    ckfwrite(state_table, state_sz, 1, f);

    /* Write out the state spillover table */
    ckfwrite(state_spill, sizeof(state_spill[0]), spill_next, f);
}

void read_table(FILE * f) {
    struct table_section_header hdr;

    /* Read in the file header */
    ckfread(&hdr, sizeof(hdr), 1, f);
    if (hdr.magic != TABLE_MAGIC) {
        fprintf(stderr, "File contains invalid table section magic (%08x != %08x)\n",
                hdr.magic, TABLE_MAGIC);
        exit(1);
    }
    if (hdr.flags != 0) {
        fprintf(stderr, "Unknown flag(s) set in table section\n");
        exit(1);
    }
    if (hdr.bitsperstate != BITSPERSTATE) {
        fprintf(stderr, "Unknown bits per state %d (!= %d)\n",
                hdr.bitsperstate, BITSPERSTATE);
        exit(1);
    }

    state_sz = hdr.statesz;
    spill_sz = hdr.spillsz;

    if ((state_table != NULL) || (state_spill != NULL)) {
        fprintf(stderr, "Internal error; refusing to overwrite existing state table.\n");
        exit(1);
    }

    /*
     * mmap() in the state table and spill table, since most positions
     * won't ever be looked at during play. Note that mmap() requires
     * the offset to be a multiple of PAGE_SIZE, so this always maps
     * from offset 0 to guarantee that happens, and that the same file
     * format will work for every PAGE_SIZE value.
     */
    uint64_t sz = state_sz + spill_sz * sizeof(state_spill[0]);
    uint64_t offset = ftell(f);
    const uint8_t * mapbase;
    struct stat fileinfo;

    fprintf(infofh, "mmap()ing  %'15ld bytes for state and spill tables...", sz);
    if (fstat(fileno(f), &fileinfo) == -1) {
        fprintf(infofh, "failed\n");
        fprintf(stderr, "Cannot fstat() file: %s\n", strerror(errno));
        exit(1);
    }
    if ((sz + offset) > fileinfo.st_size) {
        fprintf(infofh, "failed\n");
        fprintf(stderr, "State file seems to be truncated? (%ld > %ld)\n",
                sz + offset, fileinfo.st_size);
        exit(1);
    }
    mapbase = mmap(NULL, sz + offset, PROT_READ, MAP_PRIVATE, fileno(f), 0);
    if (mapbase == MAP_FAILED) {
        fprintf(infofh, "failed\n");
        fprintf(stderr, "Error: %s mmap()ing state table\n", strerror(errno));
        exit(1);
    }
    fprintf(infofh, "OK\n");

    state_table = (uint64_t *)(mapbase + offset);
    state_spill = (uint16_t *)(mapbase + offset + state_sz);
    if (spill_sz == 0) {
        /* Spill size of 0 means use simple mode */
        simple_state_table = (uint8_t *)state_table;
    }

    if (fseek(f, sz, SEEK_CUR) != 0) {
        fprintf(stderr, "Error: cannot fseek() in file: %s\n", strerror(errno));
        exit(1);
    }
}

// Local Variables:
// rmsbolt-command: "gcc-9.3 -O3 -g -ggdb3 -c -march=native -fverbose-asm"
// rmsbolt-disassemble: nil
// rmsbolt-asm-format: "att"
// End:
