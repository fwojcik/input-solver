/*
 * solve-input, a solver for the board game of Input(tm)
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * This program is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License
 * version 3 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * See the LICENSE file for more details.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <locale.h>

#include "game.h"
#include "staterep.h"
#include "index.h"
#include "table.h"
#include "io.h"

#include "rng.h"

/*
 * This file implements a simple, basic engine for playing Input(tm)
 * using the data from the game solver. It is primarily a technology
 * and API demo to show how the data can be used, and to explore the
 * game's solution space.
 *
 * This code plays both sides of an Input(tm) game, choosing moves for
 * each player according to a set of very simple strategies.
 */

enum outcome_t {
    WE_LOSE,
    WE_DRAW,
    WE_WIN
};

void usage(void) {
    printf("play-input, a simple client for playing solved instances of\n");
    printf("  the board game of Input(tm)\n");
    printf("\n  Usage: play-input [options] [inputfile]\n");
    printf("    If inputfile is not specified, then data is read from statetree.dat\n");
    printf("\n  Game strategy options:\n");
    printf("    --forceloss\tForces the first move to be a losing one, if any exist\n");
    printf("    --random\tMakes players choose random moves instead of the best ones\n");
    printf("\n  RNG option:\n");
    printf("    --seed N\tUse the specified seed for generating random numbers\n");
    printf("\n  Other option:\n");
    printf("    --maxturns N\tAllow the game to proceed for up to N turns\n");
    printf("                \tBy default, the game will be ended at 200 turns\n");
}

int main(int argc, const char * argv[]) {
    /* Unbuffer stdout always */
    setbuf(stdout, NULL);
    /* Allow printf() to provide the thousands separator */
    setlocale(LC_NUMERIC, "");
    /* Write info messages to stdout */
    init_infofile(stdout);

    unsigned maxturns  = 200;
    unsigned forceloss = 0;
    unsigned random    = 0;
    unsigned seed      = 0;
    while ((argc >= 2) && (argv[1][0] == '-') && (argv[1][1] == '-')) {
        if (!strcmp(&argv[1][2], "help")) {
            usage();
            exit(1);
        } else if (!strcmp(&argv[1][2], "forceloss")) {
            /*
             * This makes the first move from Player 1 be as favorable
             * to Player 2 as possible.
             */
            forceloss = 1;
        } else if (!strcmp(&argv[1][2], "random")) {
            /* This makes each player choose random moves */
            random = 1;
        } else if (!strcmp(&argv[1][2], "seed")) {
            if (argc < 3) {
                fprintf(stderr, "Need to supply a seed value\n");
                exit(1);
            }
            seed = atoi(argv[2]);
            argc--; argv++;
        } else if (!strcmp(&argv[1][2], "maxturns")) {
            /* Specify the max game length */
            if (argc < 3) {
                fprintf(stderr, "Need to supply a maximum game length\n");
                exit(1);
            }
            maxturns = atoi(argv[2]);
            argc--; argv++;
        } else if (argv[1][2] != '\0') {
            fprintf(stderr, "Unknown option: %s\n", argv[1]);
            exit(1);
        } else {
            argc--; argv++;
            break;
        }
        argc--; argv++;
    }

    rng_seed(seed);
    rng_init(0);

    /* Allow users to specify a solved state file */
    if (argc >= 2) {
        read_state_table(argv[1]);
    } else {
        read_state_table(NULL);
    }

    /*
     * Allow users to specify a starting position as a placenum. Otherwise,
     * the game begins with all pieces on the HOMEROW.
     */
    uint64_t startpos = 0;
    if (argc >= 3) {
        uint64_t startplace = 0;
        if (strspn(argv[2], "0123456789") != 2*PIECES) {
            printf("Invalid starting placenum %s\n", argv[2]);
            exit(1);
        }
        for (unsigned p = 0; p < 2 * PIECES; p++) {
            startplace = set(startplace, p, argv[2][p] - '0');
        }
        startpos = compute_piecepos(startplace);
    } else {
        for (unsigned p = 0; p < PIECES; p++) {
            startpos = set(startpos, p, HOMEROW);
            startpos = set(startpos, p + PIECES, MIRROR(HOMEROW));
        }
    }

    enum outcome_t best_outcome;
    uint16_t fcount, results[2 * PIECES];
    uint64_t fbuffer[2 * PIECES];
    uint64_t turn = 0;

    uint64_t pos = startpos;
    do {
        /* Stop the game at the user-defined point */
        if (turn >= maxturns) {
            printf("Stopping after %d turns\n", maxturns);
            break;
        }
        turn++;

        /* Get the game theoretic result of the current position */
        uint64_t placenum = compute_placenum(pos);
        uint64_t posidx   = compute_posidx(pos);
        uint16_t state    = table_get(posidx);
        printf("Turn %ld position %s -- placenum %s -- idx %ld -- %s %d\n",
                turn, posstr(pos), placestr(placenum), posidx,
                state_type_str(state), decode_state_num(state));

        /*
         * Evaluate all legal moves, and get the game theoretic result
         * of making them. Remember that after the current player
         * makes a move it will be the opponent's turn, so the result
         * value *is from the perspective of the opponent*.
         */
        best_outcome = WE_LOSE;
        fcount = forward_moves(pos, fbuffer);
        for (uint16_t j = 0; j < fcount; j++) {
            placenum   = compute_placenum(fbuffer[j]);
            posidx     = compute_posidx(fbuffer[j]);
            state      = table_get(posidx);
            results[j] = state;
            printf("\t Move %d -- %s -- placenum %s -- idx %ld -- %s %d\n",
                    j, posstr(fbuffer[j]), placestr(placenum), posidx,
                    state_type_str(state), decode_state_num(state));

            if (decode_state_type(state) == LOSS) {
                /*
                 * If our opponent can be forced into a losing
                 * position then we win.
                 */
                best_outcome = WE_WIN;
            }
            if ((best_outcome == WE_LOSE) && (decode_state_type(state) == COUNT)) {
                /*
                 * If we find even one move that makes our opponent's
                 * best case be a draw, then we aren't in a forced-loss
                 * position, so can't do worse than a draw.
                 */
                best_outcome = WE_DRAW;
            }
        }

        /*
         * Choose among the moves found, according to the strategy
         * specified.
         */
        uint16_t count = 0;
        int16_t chosen_move = -1;
        if (forceloss && (turn == 1)) {
            printf("\tForcing a losing move\n");
            for (int16_t j = 0; j < fcount; j++) {
                if (decode_state_type(results[j]) == WIN) {
                    if (rng_range(++count) == 0) {
                        chosen_move = j;
                    }
                }
            }
            if (chosen_move == -1) {
                printf("No losing moves found!\n");
                exit(1);
            }
        } else if (random) {
            chosen_move = rng_range(fcount);
            printf("\tFound %d moves: choosing at random among them\n", fcount);
        } else if (best_outcome == WE_WIN) {
            /*
             * We found at least one way to make the opponent lose;
             * choose a move which ends the game the quickest (lowest
             * Distance To Win).
             */
            uint16_t mindtw = -1;
            for (int16_t j = 0; j < fcount; j++) {
                if (decode_state_type(results[j]) != LOSS) {
                    continue;
                }
                uint16_t dtw = decode_state_num(results[j]);
                if (mindtw > dtw) {
                    mindtw = dtw;
                    count = 1;
                    chosen_move = j;
                } else if ((mindtw == dtw) && (rng_range(++count) == 0)) {
                    chosen_move = j;
                }
            }
            printf("\tFound a winning move: best is DTW %d\n", mindtw);
        } else if (best_outcome == WE_DRAW) {
            /*
             * We found at least one way to make the opponent's best
             * case be a draw; choose the move which has the fewest
             * resulting drawing moves, which gives them more chances
             * to make a mistake and choose a losing move instead.
             */
            uint16_t mindist = -1;
            for (int16_t j = 0; j < fcount; j++) {
                if (decode_state_type(results[j]) != COUNT) {
                    continue;
                }
                uint16_t drawdist = decode_state_num(results[j]);
                if (mindist > drawdist) {
                    mindist = drawdist;
                    count = 1;
                    chosen_move = j;
                } else if ((mindist == drawdist) && (rng_range(++count) == 0)) {
                    chosen_move = j;
                }
            }
            printf("\tFound a drawing move: best has %d drawing response(s)\n", mindist);
        } else {
            /*
             * We found only moves that can let the opponent win; we
             * are in a forced-loss position, assuming perfect play.
             * Choose the move which ends the game the slowest
             * (highest Distance To Win), which gives them more
             * chances to make a mistake and choose a losing or
             * drawing move instead.
             */
            uint16_t maxdtw = 0;
            for (int16_t j = 0; j < fcount; j++) {
                if (decode_state_type(results[j]) != WIN) {
                    continue;
                }
                uint16_t dtw = decode_state_num(results[j]);
                if (maxdtw < dtw) {
                    maxdtw = dtw;
                    count = 1;
                    chosen_move = j;
                } else if ((maxdtw == dtw) && (rng_range(++count) == 0)) {
                    chosen_move = j;
                }
            }
            printf("\tFound only losing moves: least bad is DTW %d\n", maxdtw);
        }

        printf("\tTaking move %d\n", chosen_move);
        if (chosen_move < 0) {
            fprintf(stderr, "Internal error\n");
            exit(1);
        }
        pos = fbuffer[chosen_move];

    } while (game_state(pos) == GAME_INPROGRESS);

    /*
     * If the turn number is even, then player 2 just moved, so pos
     * describes the board state at the start of a turn where player 1
     * is the player-to-move, so game_state describes player 1's
     * result. And vice-versa if the turn number is odd.
     */
    printf("Complete! Game for player %d is %s in pos %s.\n",
            ((turn % 2) == 0) ? 1 : 2, game_statestr(game_state(pos)), posstr(pos));
    exit(0);
}
