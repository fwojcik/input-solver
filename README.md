# Input&trade; solver

This is an engine for playing and solving the Input&trade; board game.

Input&trade; is a chess-like game where two players alternate taking
turns by moving one of their pieces onto any legal square. Pieces have
a "pre-programmed" path printed on them that they must follow in
sequence. If a player's piece lands on the same square as an
opponent's piece, that piece is captured and removed from the
board. When all pieces of one player have been captured, that player
loses the game.

More information on the board game is available [on
BoardGameGeek](https://boardgamegeek.com/boardgame/1291/input).

## Features

This project has an implementation of the game rules, a solver that
uses retrograde analysis to find the game-theoretic values for all
reachable game states, as well as Distance To Win values, and a very
simple client to demonstrate how to play the game using the solution
data.

It does not yet have any options for interactive human play.

## Notes on game rules

While the Input&trade; rulebook is very good in most respects, there
is one small problem with it. It does not explicitly specify what
happens when it is a player's turn, they have pieces on the board, but
they have no legal moves. It does state that "ON EACH TURN YOU MUST
MOVE ONE OF YOUR PLAYING PIECES FROM ONE SPACE TO ANOTHER SPACE!" but
does not state what to do if that is impossible.

Taking a cue from chess and other chess-like games, I have decided
that the engine will handle this case by making that player lose.

The default game rules allow up to three pieces to be stacked on top
of each other on the "Entering space". This engine implements some
minor variations for this rule. The number of pieces allowed on the
space (referred to informally as "the stack") can be 0, 1, 2, or 3. If
0 pieces are allowed on the stack, then they are allowed to move
directly from the home row (the "back space" in the rules) onto their
starting space on the board.

## Quick Start installation

1. `git clone https://gitlab.com/fwojcik/input-solver.git`
1. `cd input-solver`
1. `make` or `gmake`, as needed on your system
1. `git clone https://gitlab.com/fwojcik/input-solutions.git` (this step may take quite some time)
1. `xz -d input-solutions/statetree.1.dat.xz`

## Usage

1. `./play-input input-solutions/statetree.1.dat`

## Notes on game solution data

Sadly, gitlab does not allow for very large repositories, so the only
solutions that can be hosted there are for stack heights of zero and
one. In order to get a complete game solution, you will need to run
`solve-input` yourself.

Here are the checksums of the different solution files that were
generated on my machine:

    $ md5sum statetree.*
    f3226245dc6d6400552adf6b9490b8df  statetree.0.dat
    7f984fa27b4087fb4cb92514296e460e  statetree.1.dat
    354c722591d682521e9c833f531640e1  statetree.2.dat
    d8efe2b9112b41d4c669b1cb6a1911cc  statetree.3.dat

## Using play-input

The Quick Start steps will allow you to see games played using the
solution state tree. Interesting options are `--forceloss` which makes
Player 1 choose a losing first move, and `--random` and `--seed N`
which makes players take random moves. `--maxturns N` allows shorter
or longer games.

## Using solve-input and verify-solve-input

If you want to run the solver yourself, to see how the solution data
was generated, you can just run `solve-input`. You can specify
`--stackheight N` to specify the number of pieces which are allowed
onto the "stack" space in the game. Smaller numbers lead to smaller
solution spaces, which means the solver runs faster and uses less
memory. Compressed solution files for stack heights of 0 and 1 are in
the `input-solutions` repository.

`verify-solve-input` is a superset of `solve-input`, where it does a
number of additional self-tests. These take significant time to run,
but do not increase the memory requirements. The solution state files
it produces are bitwise-identical to those from `solve-input`.

Be warned that the solver takes about 39 GB of memory and several
hours to run. Here is the approximate memory usage depending on the
stack height, and the runtimes taken on the author's 3950X CPU.

| Stack height | Queue size | Table size | Total memory |
| :---: | :---: | :---: | :---:  |
| 3     | 17 GB | 21 GB | 39 GB  |
| 2     | 17 GB | 13 GB | 31 GB  |
| 1     |  4 GB |  5 GB | 10 GB  |
| 0     |  1 GB |  1 GB | 2.5 GB |

| Stack height | solve runtime | verify-solve runtime |
| :---: |  :---:  |  :---:   |
| 3     |    4 hr | 9.15 hr  |
| 2     | 2.25 hr |  5.5 hr  |
| 1     |  44 min |    2 hr  |
| 0     | 7.5 min | 22.5 min |

You can use the `--mmap-queue` and/or `--mmap-table` options to use
disk space for one or both of the larger data structures, but this can
come at a significant increase in runtime.

Another time/memory tradeoff is the CACHE_PLACENUM #define. Undefining
it will halve the memory requirements of the queue at the cost of
recomputing numbers that would have been cached instead.

`--mmap-queue` likely has the smallest impact compared to the other
approaches to reducing memory.

## Roadmap

The biggest feature-level things that I'd like to add are (in nearly
most-to-least important order):

* Write a paper detailing some findings about the game itself
* Improving performance
* More unit tests, especially ones involving explicit board states
* An ability to play the game against a human in text mode
* An ncurses-based playing mode
* A GUI for playing the game
* Integration with VASSAL

## Acknowledgments

- [G�bor E. G�vay and G�bor Danner's paper on ultra-strong solutions for Nine Men's Morris](https://arxiv.org/abs/1408.0032)
- [Succinct Data Structure Library 2.0](https://github.com/simongog/sdsl-lite)

- [xoshiro256** RNG](http://prng.di.unimi.it/xoshiro256starstar.c), and [details on a weakness](https://www.pcg-random.org/posts/xoshiro-repeat-flaws.html)
- [Jurgen Doornik's paper on floating point numbers (PDF)](https://www.doornik.com/research/randomdouble.pdf)
- [Debasis Kundu and Rameshwar D. Gupta's paper on generating random gamma variables (PDF)](http://home.iitk.ac.in/~kundu/paper120.pdf)

- [valgrind](https://www.valgrind.org/)

- [Coverity Scan](https://scan.coverity.com/)

<a href="https://scan.coverity.com/projects/input-solver">
  <img alt="Coverity Scan Build Status"
       src="https://img.shields.io/coverity/scan/23673.svg"/>
</a>

## Support

https://gitlab.com/fwojcik/input-solver should be the home of this
project, and I expect to use the bug tracker there.

## Contributing

Contributions are very welcome! Especially items on the Roadmap list above.

## License

Input-solver is licensed under the GNU Affero General Public License
Version 3. See the `LICENSE` file for more information.

    input-solver, a solver for the board game of Input
    Copyright (C) 2021  Frank J. T. Wojcik

    This program is free software: you can redistribute it and/or
    modify it under the terms of the GNU Affero General Public License
    version 3 as published by the Free Software Foundation.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

## Disclaimer

Input&trade; is a trademark of Milton Bradley. Use of the trademark
does not imply any affiliation with or endorsement by them.

## File contents

### Main code
<dl>
<dt>solve.c</dt>
<dd>The main retrograde analysis algorithm, and functions to iterate over all board states</dd>

<dt>play.c</dt>
<dd>A very simple use of the API to play games using the solution data</dd>

<dt>game.c</dt>
<dd>Implementation of the game rules and move generation</dd>

<dt>index.c</dt>
<dd>A minimal perfect hash function for mapping game states to ordinal indices</dd>

<dt>forest.c</dt>
<dd>A set of routines for succinctly packing trees derived from board states, and counting their leaf nodes</dd>

</dl>

### Utility code
<dl>
<dt>staterep.h</dt>
<dd>A coding scheme for packing data into integers, used by solve.c and play.c</dd>

<dt>table.c</dt>
<dd>Implementation of an array optimized for storing many small values and some larger values</dd>

<dt>queue.c</dt>
<dd>A simple fixed-size circular queue</dd>

<dt>io.c</dt>
<dd>Top-level file reading/writing functions</dd>

<dt>rng.c</dt>
<dd>Fast, small, self-contained RNG and related functions</dd>
</dl>
