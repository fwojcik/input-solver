/*
 * Copyright (C) 2021  Frank J. T. Wojcik
 *
 * See the LICENSE file for more details.
 */
void alloc_table(size_t state_count, size_t init_spill_count, unsigned use_mmap);
uint16_t table_get(uint64_t slotnum);
void table_set(uint64_t slotnum, uint16_t value);

uint64_t table_spillcnt(void);

void alloc_stub_tables(void);
void free_stub_tables(void);

void table_sanity_pre(void);
void table_sanity_post(size_t poscnt);

void write_table(FILE * f);
void read_table(FILE * f);
